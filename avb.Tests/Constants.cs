﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace avb.Tests
{
    public static class Constants
    {
#if integration
        public const string SERVER_URL = "http://local.beesightsoft.com:5502";
#elif staging
        public const string SERVER_URL = "http://soccermeshv2-appserver.azurewebsites.net";
#elif production
        public const string SERVER_URL = "http://production_url";
#else
        //public const string SERVER_URL = "http://local.beesightsoft.com:5502";
        public const string SERVER_URL = "http://localhost:5801";
#endif
        public const string CONTROLLER_TEST_CATEGORY = "controller_tests";
        public const string INTEGRATION_TEST_CATEGORY = "integration_tests";
        public const string REGRESSION_TEST_CATEGORY = "regression_tests";
        public const string CONFIRMATION_TEST_CATEGORY = "confirmation_tests";

        public const string MOBILE_ACCEPT_HEADER = "application/vnd.app.soccermesh.mobile-v1+json";
        public const string ADMIN_ACCEPT_HEADER = "application/vnd.app.soccermesh.admin-v1+json";

    }
}
