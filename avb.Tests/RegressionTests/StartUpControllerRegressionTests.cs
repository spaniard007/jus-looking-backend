﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyTested.WebApi;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace avb.Tests.RegressionTests
{
    [TestClass]

    public class StartUpControllerRegressionTests
    {
        [TestMethod]
        [TestCategory(Constants.REGRESSION_TEST_CATEGORY)]
        [TestCategory(Constants.CONFIRMATION_TEST_CATEGORY)]
        public void TestPingServerConfirmation()
        {
            MyWebApi.Server()
                .WorkingRemotely(Constants.SERVER_URL)
                .WithHttpRequestMessage(request => request
                    .WithMethod(HttpMethod.Get)
                    .WithRequestUri("api"))
                .ShouldReturnHttpResponseMessage()
                .WithStatusCode(HttpStatusCode.OK);
        }

        [TestMethod]
        [TestCategory(Constants.REGRESSION_TEST_CATEGORY)]
        [TestCategory(Constants.CONFIRMATION_TEST_CATEGORY)]
        public void TestSetupDatabaseConfirmation()
        {
            MyWebApi.Server()
                .WorkingRemotely(Constants.SERVER_URL)
                .WithHttpRequestMessage(request => request
                    .WithMethod(HttpMethod.Get)
                    .WithRequestUri("api/database"))
                .ShouldReturnHttpResponseMessage()
                .WithStatusCode(HttpStatusCode.OK);
        }

        [TestMethod]
        [TestCategory(Constants.REGRESSION_TEST_CATEGORY)]
        [TestCategory(Constants.CONFIRMATION_TEST_CATEGORY)]
        public void TestSecureConfirmation()
        {
            var data = new {username = "minh.luu@beesightsoft.com", password = "abc123"};
            var response = MyWebApi.Server()
                .WorkingRemotely(Constants.SERVER_URL)
                .WithHttpRequestMessage(request => request
                    .WithMethod(HttpMethod.Post)
                    .WithRequestUri("api/users/login")
                    .WithJsonContent(JsonConvert.SerializeObject(data)))
                .ShouldReturnHttpResponseMessage()
                .WithStatusCode(HttpStatusCode.OK).AndProvideTheHttpResponseMessage();
            var responseData = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            var accessToken = responseData["data"]["access_token"];
            MyWebApi.Server()
                .WorkingRemotely(Constants.SERVER_URL)
                .WithHttpRequestMessage(request => request
                    .WithMethod(HttpMethod.Get)
                    .WithRequestUri("api/testsecure")
                    .WithHeader(HttpHeader.Authorization, "Bearer " + accessToken))
                .ShouldReturnHttpResponseMessage()
                .WithStatusCode(HttpStatusCode.OK);

        }


        
    }
}
