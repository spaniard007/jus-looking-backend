﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyTested.WebApi;
using Newtonsoft.Json;

namespace avb.Tests.RegressionTests
{
    [TestClass]
    public class AccountControllerRegressionTests
    {
        [TestMethod]
        [TestCategory(Constants.REGRESSION_TEST_CATEGORY)]
        [TestCategory(Constants.CONFIRMATION_TEST_CATEGORY)]
        public void TestRegisterOrLoginExternalConfirmation()
        {
            var data =
                new
                {
                    provider = "facebook",
                    externalaccesstoken =
                        "EAADMa7s4R2QBAMwwkBrMVx6MrHfQJCkDVmBcxdHmMfUd4ZArFeVuRvHXH7xdZCv6ajyRDs1TGOTVaibgJZBo4ufYEigEOVIi2pGXxiuyEqNPRzJVv3IhWjmJMSlwjeomBnJRPrrVf6H1GPmn9voPj4EyGX8tdKAQYbNMSrjmuZASgudSlRixmUzEIzq5FmbtvY2EgIfX4wZDZD"
                };
            MyWebApi.Server()
              .WorkingRemotely(Constants.SERVER_URL)
              .WithHttpRequestMessage(request => request
                  .WithMethod(HttpMethod.Post)
                  .WithRequestUri("api/users/registerOrLoginExternal")
                  .WithJsonContent(JsonConvert.SerializeObject(data)))
              .ShouldReturnHttpResponseMessage()
              .WithStatusCode(HttpStatusCode.OK);
        }

        [TestMethod]
        [TestCategory(Constants.REGRESSION_TEST_CATEGORY)]
        [TestCategory(Constants.CONFIRMATION_TEST_CATEGORY)]
        public void TestRegisterConfirmation()
        {
            var data =
                new
                {
                    email = "minh.luu3@beesightsoft.com",
                    password = "abc123"
                };
            MyWebApi.Server()
              .WorkingRemotely(Constants.SERVER_URL)
              .WithHttpRequestMessage(request => request
                  .WithMethod(HttpMethod.Post)
                  .WithRequestUri("api/users/register")
                  .WithJsonContent(JsonConvert.SerializeObject(data)))
              .ShouldReturnHttpResponseMessage()
              .WithStatusCode(HttpStatusCode.Created);
        }

        [TestMethod]
        [TestCategory(Constants.REGRESSION_TEST_CATEGORY)]
        [TestCategory(Constants.CONFIRMATION_TEST_CATEGORY)]
        public void TestLoginConfirmation()
        {
            var data = new {username = "minh.luu@beesightsoft.com", password = "abc123"};
             MyWebApi.Server()
                .WorkingRemotely(Constants.SERVER_URL)
                .WithHttpRequestMessage(request => request
                    .WithMethod(HttpMethod.Post)
                    .WithRequestUri("api/users/login")
                    .WithJsonContent(JsonConvert.SerializeObject(data)))
                .ShouldReturnHttpResponseMessage()
                .WithStatusCode(HttpStatusCode.OK);
        }

    }
}
