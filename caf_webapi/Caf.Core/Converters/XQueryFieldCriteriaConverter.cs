﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Caf.Core.Converters
{
    public class XQueryFieldCriteriaConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            // can write is set to false
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            XQueryContext criteria = new XQueryContext();

            JObject j = JObject.Load(reader);
            JProperty root = j.First as JProperty;
            criteria.And = root.Name.Equals("and", StringComparison.InvariantCultureIgnoreCase);

            foreach (var property in root.First.Children<JProperty>())
            {
                if (property.Name.Equals("field", StringComparison.InvariantCultureIgnoreCase))
                {
                    criteria.Field = property.Value.Value<string>();
                }
                else if (property.Name.Equals("op", StringComparison.InvariantCultureIgnoreCase))
                {
                    criteria.Operator = property.Value.Value<string>();
                }
                else if (property.Name.Equals("value", StringComparison.InvariantCultureIgnoreCase))
                {
                    var propertyValue = property.Value;
                    if (propertyValue.Type == JTokenType.Integer)
                    {
                        criteria.Value = property.Value.Value<int>();
                    }
                    else if (propertyValue.Type == JTokenType.Boolean)
                    {
                        criteria.Value = property.Value.Value<bool>();
                    }
                    else if (propertyValue.Type == JTokenType.String)
                    {
                        criteria.Value = property.Value.Value<string>();
                    }
                    else if (propertyValue.Type == JTokenType.Date)
                    {
                        criteria.Value = property.Value.Value<string>();
                    }
                    else if (propertyValue.Type == JTokenType.Float)
                    {
                        criteria.Value = property.Value.Value<double>();
                    }
                    else if (propertyValue.Type == JTokenType.Guid)
                    {
                        criteria.Value = property.Value.Value<string>();
                    }
                    else if (propertyValue.Type == JTokenType.Array)
                    {
                        //need to check for this, not working right now
                        criteria.Value = property.Value.Value<int[]>();
                    }
                }
            }


            return criteria;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(XQueryContext);
        }
    }
}
