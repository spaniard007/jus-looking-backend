﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Caf.Core.Models
{
    public abstract class EntitySerializableCollection<T> : Collection<T>, IEntitySerializable
    {
        public void Add(ICollection<T> collection)
        {
            foreach (var item in collection)
            {
                this.Add(item);
            }
        }

        [JsonIgnore]
        public string Serialized
        {
            get { return JsonConvert.SerializeObject(this, new JsonSerializerSettings() { Formatting = Formatting.None, ContractResolver = new CamelCasePropertyNamesContractResolver() }); }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    return;
                }

                var jData = JsonConvert.DeserializeObject<List<T>>(value);
                this.Items.Clear();
                this.Add(jData);
            }
        }
    }
}
