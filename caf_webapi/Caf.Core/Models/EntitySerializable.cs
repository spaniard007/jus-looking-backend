﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fasterflect;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Caf.Core.Models
{
    public abstract class EntitySerializable : IEntitySerializable
    {
        [JsonIgnore]
        public string Serialized
        {
            get { return JsonConvert.SerializeObject(this, new JsonSerializerSettings() { Formatting = Formatting.None, ContractResolver = new CamelCasePropertyNamesContractResolver() }); }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    return;
                }

                var jData = JsonConvert.DeserializeObject(value, this.GetType());
                var type = this.GetType();
                var propertyInfoes = type.GetProperties();
                foreach (var propertyInfo in propertyInfoes)
                {
                    if (propertyInfo.Name.Equals("Serialized", StringComparison.OrdinalIgnoreCase)) continue;
                    var propertyValue = jData.GetPropertyValue(propertyInfo.Name, Flags.Public | Flags.Instance | Flags.IgnoreCase);
                    this.SetPropertyValue(propertyInfo.Name, propertyValue);
                }
            }
        }
    }
}
