﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caf.Core.Models
{
    public abstract class GuidEntityBase : EntityBase<Guid, long>
    {
        #region Constructors
        public GuidEntityBase()
        {
            this.Id = Guid.NewGuid();
        }
        #endregion
    }
}
