﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caf.Core.Models
{
    public interface IEntity<TIdentifier>
    {
        #region Id
        TIdentifier Id { get; set; }
        #endregion
    }
}
