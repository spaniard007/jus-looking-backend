﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caf.Core.Data
{
    public class PaginationMetaData
    {
        public long? MaxId { get; set; }
        public long? SinceId { get; set; }
        public long? Count { get; set; }
        public int? Page { get; set; }
        public long? Total { get; set; }
    } 
}
