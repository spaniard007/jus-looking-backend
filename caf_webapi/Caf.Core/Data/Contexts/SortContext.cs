﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caf.Core.Data
{
    public class SortContext
    {
        #region Field
        public string Field { get; set; }
        #endregion

        #region IsAscending
        public bool IsAscending { get; set; }
        #endregion
    }
}
