﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caf.Core.Data
{
    public class QueryContext
    {
        public IEnumerable<FieldContext> Fields { get; set; }
        public IEnumerable<XQueryContext> XQueries { get; set; }
        public IEnumerable<SortContext> Sorts { get; set; }
        public PagingContext Pagination { get; set; }
        public IEnumerable<IncludeContext> Includes { get; set; }
        public string Sql { get; set; }

        public static QueryContext Build()
        {
            var queryContext = new QueryContext();
            return queryContext;
        }

        public QueryContext WithSql(string sql)
        {
            this.Sql = sql;
            return this;
        }

        public QueryContext WithFields(params string[] fields)
        {
            this.Fields = fields.Select(field => new FieldContext() {Field = field}).ToArray();
            return this;
        }

        public QueryContext WithFields(string fields)
        {
            IEnumerable<FieldContext> fieldsContext = null;
            if (!string.IsNullOrEmpty(fields))
            {
                fieldsContext = fields.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(f => new FieldContext() { Field = f });
            }
            this.Fields = fieldsContext;
            return this;
        }

        public QueryContext WithIncludes(params string[] includes)
        {
            this.Includes = includes.Select(include => new IncludeContext() { Entity = include }).ToArray();
            return this;
        }

        public QueryContext WithIncludes(string includes)
        {
            IEnumerable<IncludeContext> includesContext = null;
            if (!string.IsNullOrEmpty(includes))
            {
                includesContext = includes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(s => new IncludeContext() { Entity = s });
            }
            this.Includes = includesContext;
            return this;
        }

        public QueryContext WithPaging(int? count, int? sinceId, int? maxId, int? page)
        {
            if (page.HasValue && !count.HasValue)
            {
                count = 20;
            }

            this.Pagination = new PagingContext() { Count = count, SinceId = sinceId, MaxId = maxId, Page = page};
            return this;
        }

        public QueryContext WithPaging(int? count, int? page)
        {
            this.Pagination = new PagingContext() { Count = count, Page = page};
            return this;
        }

        public QueryContext Sort(string field, bool isAscending = true)
        {
            var sortContext = new SortContext() { Field = field, IsAscending = isAscending};
            this.Sorts = this.Sorts == null ? new[] {sortContext} : this.Sorts.Concat(new[] {sortContext});

            return this;
        }

        public QueryContext Order(string field, bool isAscending = true)
        {
            return this.Sort(field, isAscending);
        }

        public QueryContext Where(string field, string @operator, dynamic value)
        {
            var context = new XQueryContext() { And = true, Field = field, Operator = @operator, Value = value};
            this.XQueries = new[] { context };

            return this;
        }

        public QueryContext And(string field, string @operator, dynamic value)
        {
            var context = new XQueryContext() { And = true, Field = field, Operator = @operator,  Value = value };
            this.XQueries = this.XQueries == null ? new[] { context } : this.XQueries.Concat(new[] { context });

            return this;
        }

        public QueryContext And(string rawQuery)
        {
            var context = new XQueryContext() { And = true, RawQuery = rawQuery };
            this.XQueries = this.XQueries == null ? new[] { context } : this.XQueries.Concat(new[] { context });

            return this;
        }


        public QueryContext Or(string field, string @operator, dynamic value)
        {
            var context = new XQueryContext() { And = false, Field = field, Operator = @operator, Value = value };
            this.XQueries = this.XQueries == null ? new[] { context } : this.XQueries.Concat(new[] { context });

            return this;
        }

        public QueryContext Or(string rawQuery)
        {
            var context = new XQueryContext() { And = false, RawQuery = rawQuery};
            this.XQueries = this.XQueries == null ? new[] { context } : this.XQueries.Concat(new[] { context });

            return this;
        }
    }
}
