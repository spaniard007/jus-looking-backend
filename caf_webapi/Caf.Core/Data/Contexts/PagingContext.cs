﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caf.Core.Data
{
    public class PagingContext
    {
        #region Page
        public int? Page { get; set; }
        #endregion

        #region Count
        public int? Count { get; set; }
        #endregion

        #region MaxId
        public long? MaxId { get; set; } // Id <= MaxId
        #endregion 

        #region SinceId
        public long? SinceId { get; set; } // SinceId > MaxId
        #endregion

        #region SupportPaging
        public bool SupportPaging
        {
            get { return this.Page != null || this.Count != null || this.MaxId != null || this.SinceId != null; }
        }
        #endregion

        #region PaginationType
        public PaginationType PaginationType
        {
            get
            {
                if (this.Page != null) return PaginationType.Offset;
                if (this.SinceId != null || this.MaxId != null) return PaginationType.Id;
                if(this.Count != null) return PaginationType.Id;

                return PaginationType.Cursor;
            }
        }
        #endregion
    }

    public enum PaginationType
    {
        Offset,
        Id,
        Cursor
    }
}
