﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caf.Core.Data
{
    public class XQueryContext
    {
        public bool And { get; set; }
        public string Field { get; set; }
        public string Operator { get; set; }
        public dynamic Value { get; set; }

        public string RawQuery { get; set; }

        public string ValueAsString()
        {
            var result = string.Empty;

            Type valueType = this.Value.GetType();
            if (valueType == typeof(string))
            {
                result = string.Format("'{0}'", this.Value);
            }
            else
            {
                result = this.Value.ToString();
            }

            return result;
        }
    }
}
