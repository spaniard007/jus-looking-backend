﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caf.Core.Data
{
    public class QueryResult : BaseResult
    {
        public IList<dynamic> MultipleResults { get; set; }
        public dynamic SingleResult { get; set; }
        public PaginationMetaData PaginationMetada { get; set; }
    }
}
