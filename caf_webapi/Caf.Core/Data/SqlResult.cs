﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Caf.Core.Data
{
    public class SqlResult
    {
        public string Sql { get; set; }
        public DynamicParameters Parameters { get; set; }
    }
}
