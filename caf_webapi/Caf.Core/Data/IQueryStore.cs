﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Models;

namespace Caf.Core.Data
{
    public interface IQueryStore
    {
        IQueryStore Use(IDbConnection connection);
        IQueryStore Use(DbContext connection);

        QueryResult QueryAll<T>(QueryContext queryContext, IDbTransaction transaction = null);
        QueryResult QueryOne<T, TIdentifier>(TIdentifier id, QueryContext queryContext, IDbTransaction transaction = null)
            where T : IEntity<TIdentifier>;

        CreateOrUpdateResult Update<T, TIdentifier>(TIdentifier id, dynamic data, IDbTransaction transaction = null)
            where T : IEntity<TIdentifier>;
        CreateOrUpdateResult Create<T>(dynamic data, IDbTransaction transaction = null);
    }
}
