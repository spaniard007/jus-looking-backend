﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caf.Core.Data
{
    public class CreateOrUpdateResult : BaseResult
    {
        //public long Id { get; set; }
        public dynamic SingleResult { get; set; }
    }
}
