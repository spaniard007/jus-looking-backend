﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Data.TypeCache;
using Caf.Core.Extensions;
using Caf.Core.Models;
using Dapper;
using Fasterflect;
using Newtonsoft.Json.Linq;

namespace Caf.Core.Data
{
    public class QueryStore : IQueryStore
    {
        #region Fields
        private IDbConnection connection = null;
        private static CultureInfo CultureInfo = new CultureInfo("en-US");
        #endregion

        protected ITypeDescriptorCache TypeDescriptorCache { get; set; }

        #region QueryBuilder
        public IQueryBuilder QueryBuilder { get; private set; }
        #endregion

        #region Use
        public IQueryStore Use(IDbConnection connection)
        {
            this.connection = connection;
            return this;
        }
        #endregion

        #region Use Context
        public IQueryStore Use(DbContext connection)
        {
            this.connection = connection.Database.Connection;
            return this;
        }
        #endregion

        #region Query All
        public QueryResult QueryAll<T>(QueryContext queryContext, IDbTransaction transaction = null)
        {
            if (this.connection == null) throw new ArgumentNullException("DbConnection has not been initialized.");
            string sql = this.QueryBuilder.SqlForQueryAll<T>(queryContext);

            PaginationMetaData paginationMetaData = null;

            List<dynamic> entities = null;
            if (queryContext.Includes == null)
            {
                if (queryContext.Pagination != null && queryContext.Pagination.SupportPaging)
                {
                    var resultSet = connection.QueryMultiple(sql);
                    entities = resultSet.Read().ToList();

                    paginationMetaData = new PaginationMetaData();
                    paginationMetaData.Page = queryContext.Pagination.Page;
                    paginationMetaData.Count = queryContext.Pagination.Count;
                    if (queryContext.Pagination.PaginationType == PaginationType.Id)
                    {
                        var paginationRow = resultSet.ReadFirstOrDefault<PaginationRow>();
                        paginationMetaData.SinceId = paginationRow.SinceId;
                        paginationMetaData.MaxId = paginationRow.MaxId;
                    }
                    else if (queryContext.Pagination.PaginationType == PaginationType.Offset)
                    {
                        var paginationRow = resultSet.ReadFirstOrDefault<PaginationRow>();
                        paginationMetaData.Total = paginationRow.Total;
                    }
                }
                else
                {
                    entities = connection.Query<dynamic>(sql, transaction).ToList();
                }
            }
            else
            {
                //var resultSet = connection.QueryMultiple(sql);
                //entities = resultSet.Read().ToList();

                //foreach (var include in queryContext.Includes)
                //{
                //    List<dynamic> list = resultSet.Read().ToList();
                //    IDictionary<dynamic, List<dynamic>> dictionary =
                //        list.GroupBy(item => item.SessionId).ToDictionary(g => g.Key, g => g.ToList());

                //    foreach (var pair in dictionary)
                //    {
                //        var entity = (IDictionary<string, object>)entities.FirstOrDefault(e => e.Id == pair.Key);
                //        if (entity != null)
                //        {
                //            entity.Add(include.Entity, pair.Value);
                //        }
                //    }
                //}

                var resultSet = connection.QueryMultiple(sql, transaction);
                entities = resultSet.Read().ToList();

                foreach (var include in queryContext.Includes)
                {
                    var foreignKey = string.Format("{0}Id", StringExtensions.UppercaseFirst(include.Entity));

                    List<dynamic> list = resultSet.Read().ToList();
                    foreach (IDictionary<string, object> entity in entities)
                    {
                        foreach (IDictionary<string, object> item in list)
                        {
                            if (entity[foreignKey] != null && entity[foreignKey].Equals(item["Id"]))
                            {
                                entity.Add(include.Entity, item);
                                break;
                            }
                        }
                    }
                }

                if (queryContext.Pagination != null && queryContext.Pagination.SupportPaging)
                {
                    paginationMetaData = new PaginationMetaData();
                    paginationMetaData.Page = queryContext.Pagination.Page;
                    paginationMetaData.Count = queryContext.Pagination.Count;
                    if (queryContext.Pagination.PaginationType == PaginationType.Id)
                    {
                        var paginationRow = resultSet.ReadFirstOrDefault<PaginationRow>();
                        paginationMetaData.SinceId = paginationRow.SinceId;
                        paginationMetaData.MaxId = paginationRow.MaxId;
                    }
                    else if (queryContext.Pagination.PaginationType == PaginationType.Offset)
                    {
                        var paginationRow = resultSet.ReadFirstOrDefault<PaginationRow>();
                        paginationMetaData.Total = paginationRow.Total;
                    }
                }
            }

            foreach (var entity in entities)
            {
                TreatSerializedFields<T>(entity);
            }

            var queryResult = new QueryResult();
            queryResult.MultipleResults = entities;
            queryResult.PaginationMetada = paginationMetaData;
            queryResult.Sql = sql;

            return queryResult;
        }
        #endregion

        #region Query One
        public QueryResult QueryOne<T, TIdentifier>(TIdentifier id, QueryContext queryContext, IDbTransaction transaction = null)
            where T : IEntity<TIdentifier>
        {
            if (this.connection == null) throw new ArgumentNullException("DbConnection has not been initialized.");
            string sql = this.QueryBuilder.SqlForQueryOne<T, TIdentifier>(id, queryContext);

            dynamic entity = null;
            if (queryContext.Includes == null)
            {
                entity = connection.QueryFirstOrDefault(sql, transaction);
            }
            else
            {
                var resultSet = connection.QueryMultiple(sql, transaction);
                IDictionary<string, object> data = resultSet.ReadSingle();
                IDictionary<string, object> entity2 = new Dictionary<string, object>();
                foreach (var pair in data)
                {
                    entity2.Add(pair);
                }

                foreach (var include in queryContext.Includes)
                {
                    dynamic item = resultSet.ReadSingle();
                    entity2.Add(include.Entity, item);
                }
                entity = entity2;
            }

            if (entity != null)
            {
                TreatSerializedFields<T>(entity);    
            }           

            var queryResult = new QueryResult();
            queryResult.SingleResult = entity;
            queryResult.Sql = sql;

            return queryResult;
        }
        #endregion

        private void TreatSerializedFields<T>(dynamic entity)
        {
            IDictionary<string, object> data = (IDictionary<string, object>)entity;
            //for (int i = 0; i < data.Count; i++)
            //{
            //    var pair = data.;
            //}
            foreach (var pair in data.ToList())
            {
                var key = pair.Key;
                var value = pair.Value;

                var property =
                    TypeDescriptorCache.GetTypeDescriptor<T>().SerializedFields.FirstOrDefault(
                        f => f.Name.Equals(key, StringComparison.Ordinal));

                if (property != null && value != null)
                {
                    var propertyType = property.PropertyType;
                    IEntitySerializable serializedEntity = (IEntitySerializable)propertyType.CreateInstance();
                    serializedEntity.Serialized = value.ToString();

                    data[key] = serializedEntity;
                }
            }
        }

        #region Update
        public CreateOrUpdateResult Update<T, TIdentifier>(TIdentifier id, dynamic data, IDbTransaction transaction = null)
            where T : IEntity<TIdentifier>
        {
            if (this.connection == null) throw new ArgumentNullException("DbConnection has not been initialized.");

            SqlResult result = this.QueryBuilder.SqlForUpdate<T, TIdentifier>(id, data);
            var sql = result.Sql;
            var parameters = result.Parameters;

            var resultSet = connection.QueryMultiple(sql, parameters, transaction);
            dynamic entity = resultSet.ReadSingle();
            TreatSerializedFields<T>(entity);

            return new CreateOrUpdateResult() { Sql = sql, SingleResult = entity };
        }
        #endregion

        #region Create
        public CreateOrUpdateResult Create<T>(dynamic data, IDbTransaction transaction = null)
        {
            if (this.connection == null) throw new ArgumentNullException("DbConnection has not been initialized.");

            SqlResult result = this.QueryBuilder.SqlForCreate<T>(data);
            var sql = result.Sql;
            var parameters = result.Parameters;

            var resultSet = connection.QueryMultiple(sql, parameters, transaction);
            dynamic entity = resultSet.ReadSingle();
            TreatSerializedFields<T>(entity);

            return new CreateOrUpdateResult() { Sql = sql, SingleResult = entity };
        }
        #endregion

        #region Constructors
        public QueryStore(IQueryBuilder queryBuilder)
        {
            this.QueryBuilder = queryBuilder;
            this.TypeDescriptorCache = queryBuilder.TypeDescriptorCache;
        }

        public QueryStore() : this(new SqlServerQueryBuilder(new TypeDescriptorCache()))
        {
        }
        #endregion
    }

    public class PaginationRow
    {
        public long SinceId { get; set; }
        public long MaxId { get; set; }
        public long Total { get; set; }
    }
}
