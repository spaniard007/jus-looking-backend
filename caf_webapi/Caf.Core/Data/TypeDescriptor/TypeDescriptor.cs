﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Caf.Core.Data.TypeCache
{
    public class TypeDescriptor
    {
        #region TableName
        public string TableName { get; set; }
        #endregion

        #region IdAsGuid
        public bool IdAsGuid { get; set; }
        #endregion

        #region SerializedFields
        private IList<PropertyInfo> mSerializedFields = new List<PropertyInfo>();

        public IList<PropertyInfo> SerializedFields
        {
            get { return mSerializedFields; }
            set { mSerializedFields = value; }
        }
        #endregion

        #region AuditFields
        private IList<PropertyInfo> mAuditFields = new List<PropertyInfo>();

        public IList<PropertyInfo> AuditFields
        {
            get { return mAuditFields; }
            set { mAuditFields = value; }
        }
        #endregion
    }
}
