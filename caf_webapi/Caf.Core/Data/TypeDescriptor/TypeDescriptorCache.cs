﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Models;

namespace Caf.Core.Data.TypeCache
{
    public class TypeDescriptorCache : ITypeDescriptorCache
    {
        private ConcurrentDictionary<Type, TypeDescriptor> internalCache = new ConcurrentDictionary<Type, TypeDescriptor>();

        public TypeDescriptor GetTypeDescriptor(Type entityType)
        {

            return null;
        }

        public TypeDescriptor GetTypeDescriptor<T>()
        {
            TypeDescriptor typeDescriptor;

            Type entityType = typeof(T);
            if (!internalCache.TryGetValue(entityType, out typeDescriptor))
            {
                typeDescriptor = new TypeDescriptor();
                typeDescriptor.TableName = entityType.Name;
                typeDescriptor.SerializedFields =
                    entityType.GetRuntimeProperties()
                        .Where(prop => typeof(IEntitySerializable).IsAssignableFrom(prop.PropertyType))
                        .ToList();
                typeDescriptor.AuditFields = typeof(IAudit).GetRuntimeProperties().ToList();
                typeDescriptor.IdAsGuid = true; //hacking now, implement later

                internalCache.TryAdd(entityType, typeDescriptor);
            }

            return typeDescriptor;
        }
    }
}
