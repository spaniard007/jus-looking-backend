﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caf.Core.Data.TypeCache
{
    public interface ITypeDescriptorCache
    {
        TypeDescriptor GetTypeDescriptor(Type entityType);
        TypeDescriptor GetTypeDescriptor<T>();
    }
}
