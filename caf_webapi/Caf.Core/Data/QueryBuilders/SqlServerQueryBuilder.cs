﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Data.TypeCache;
using Caf.Core.Extensions;
using Caf.Core.Models;
using Dapper;
using Newtonsoft.Json.Linq;

namespace Caf.Core.Data
{
    public class SqlServerQueryBuilder : IQueryBuilder
    {
        #region SqlForQueryAll
        public string SqlForQueryAll<T>(QueryContext queryContext)
        {
            #region Check IdAsGuid
            var nameId = TypeDescriptorCache.GetTypeDescriptor<T>().IdAsGuid ? "SecondaryId" : "Id";
            #endregion

            if (string.IsNullOrEmpty(queryContext.Sql))
            {
                #region Normal SQL
                //SELECT [FIELDS] FROM [TABLENAME] [WHERE CONDITION] [ORDER BY FIELD1,FIELD2 ASC]

                var fields = string.Empty;
                var condition = string.Empty;
                var sort = string.Empty;

                //fields
                if (queryContext.Fields == null)
                {
                    fields = "*";
                }
                else
                {
                    fields = string.Format("Id, {0}", String.Join(",", queryContext.Fields.Select(ctx => ctx.Field)));
                }

                //tableName
                var tableName = typeof(T).Name;

                //condition
                if (queryContext.XQueries != null && queryContext.XQueries.Any())
                {
                    condition = SqlForXQuery(queryContext.XQueries);
                }

                //order
                if (queryContext.Sorts != null && queryContext.Sorts.Any())
                {
                    sort = SqlForSort(queryContext.Sorts);
                }
                else
                {
                    sort = string.Format("ORDER BY {0}", nameId);
                }

                //paging
                //            SELECT *  FROM
                //  (SELECT  ROW_NUMBER() OVER (ORDER BY Id ASC) as RowNumber,*
                //  FROM Session) AS Session
                //WHERE RowNumber BETWEEN 11 AND 20


                var mainSql = string.Empty;
                var paginationSql = string.Empty;

                var pagination = queryContext.Pagination;
                if (pagination == null || !pagination.SupportPaging)
                {
                    //mainSql = string.IsNullOrEmpty(condition) ? string.Format("SELECT {0} FROM {1} {2}", fields, tableName, sort) : string.Format("SELECT {0} FROM {1} WHERE {2} {3}", fields, tableName, condition, sort);
                    mainSql = string.Format("SELECT {0} FROM {1} {2} {3}", fields, tableName, string.IsNullOrEmpty(condition) ? string.Empty : string.Format("WHERE {0}", condition), sort);
                }
                else
                {
                    if (pagination.PaginationType == PaginationType.Offset)
                    {
                        //1, 4 => 0 to 4
                        //2, 4 => 4 to 
                        //3, 4
                        var startIndex = (queryContext.Pagination.Page - 1) * queryContext.Pagination.Count;
                        var endIndex = startIndex + queryContext.Pagination.Count;

                        //SELECT * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY Id) AS Row FROM Session WHERE Id In (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)) As cols WHERE Row BETWEEN 10 AND 20
                        mainSql = string.Format("SELECT * FROM (SELECT {0}, ROW_NUMBER() OVER ({1}) AS Row FROM {2} {3}) As {4} WHERE Row BETWEEN ({5} + 1) AND {6}", fields, sort, tableName, string.IsNullOrEmpty(condition) ? string.Empty : string.Format("WHERE {0}", condition), tableName, startIndex, endIndex);
                        paginationSql = string.Format("SELECT COUNT({0}) AS Total FROM {1} {2}", nameId, tableName, string.IsNullOrEmpty(condition) ? string.Empty : string.Format("WHERE {0}", condition));
                    }
                    else if (pagination.PaginationType == PaginationType.Id)
                    {
                        var sinceId = pagination.SinceId;
                        var maxId = pagination.MaxId;
                        var count = pagination.Count.Value;

                        //Id > sinceId AND Id <= maxId
                        var sinceSql = sinceId.HasValue ? string.Format("{0} > {1}", nameId, sinceId.Value) : string.Format("{0} > 0", nameId);
                        var maxSql = maxId.HasValue ? string.Format("{0} <= {1}", nameId, maxId.Value) : string.Empty;
                        var sinceMaxSql = string.Empty;
                        if (sinceId.HasValue && maxId.HasValue)
                        {
                            sinceMaxSql = string.Format("({0} AND {1})", sinceSql, maxSql);
                        }
                        else
                        {
                            sinceMaxSql = string.Format("({0})", sinceId.HasValue ? sinceSql :
                                maxId.HasValue ? maxSql : sinceSql);

                        }

                        condition = string.IsNullOrEmpty(condition) ? string.Format("WHERE {0}", sinceMaxSql) : string.Format("WHERE {0} AND {1}", condition, sinceMaxSql);
                        mainSql = string.Format("SELECT TOP {0} {1} FROM {2} {3} {4}", count, fields, tableName, condition, sort);
                        //paginationSql = string.Format("SELECT MIN(Id) AS SinceId, MAX(Id) As MaxId FROM {0} {1} {2}", tableName, condition, sort);
                        paginationSql = string.Format("SELECT MIN({0}.{1}) AS SinceId, MAX({0}.{1}) As MaxId FROM ({2}) AS {0}", tableName, nameId, mainSql);
                    }
                    else if (pagination.PaginationType == PaginationType.Cursor)
                    {
                        throw new NotImplementedException("Cursor paging is not yet supported.");
                    }
                }

                var sqlBuilder = new StringBuilder(mainSql);

                //includes
                //if (queryContext.Includes != null)
                //{
                //    foreach (var includeContext in queryContext.Includes)
                //    {
                //        var indexOfFrom = mainSql.IndexOf("FROM", StringComparison.InvariantCultureIgnoreCase);
                //        var temp = mainSql.Substring(6, indexOfFrom - 6);

                //        sqlBuilder.AppendLine();
                //        sqlBuilder.Append(string.Format("SELECT * FROM {0} WHERE {1}Id In ({2})", includeContext.Entity, tableName, mainSql.Replace(temp, " Id ")));
                //    }
                //}

                //includes
                if (queryContext.Includes != null)
                {
                    foreach (var includeContext in queryContext.Includes)
                    {
                        var entityName = includeContext.Entity;
                        var propertyInfo = typeof(T).GetProperty(entityName, BindingFlags.Public | BindingFlags.IgnoreCase | BindingFlags.Instance);

                        if (propertyInfo != null)
                        {
                            var childTableName = propertyInfo.PropertyType.Name;

                            //var indexOfFrom = mainSql.IndexOf("FROM", StringComparison.InvariantCultureIgnoreCase);
                            //var temp = mainSql.Substring(6, indexOfFrom - 6);

                            sqlBuilder.AppendLine();
                            //sqlBuilder.Append(string.Format("SELECT * FROM {0} WHERE Id In ({1})", childTableName, mainSql.Replace(fields, string.Format(" {0}Id ", entityName))));
                            sqlBuilder.Append(string.Format("SELECT * FROM {0} WHERE {1} In ({2})", childTableName, "Id", mainSql
                                .Replace(fields, string.Format(" {0}Id ", entityName))));

                            if (pagination == null || !pagination.SupportPaging)
                            {
                                sqlBuilder.Replace(sort, string.Empty);
                            }
                        }
                    }
                }

                sqlBuilder.AppendLine();
                sqlBuilder.Append(paginationSql);

                var sql = sqlBuilder.ToString();
                return sql;
                #endregion
            }
            else
            {
                #region Custom SQL
                //SELECT [FIELDS] FROM [TABLENAME] [WHERE CONDITION] [ORDER BY FIELD1,FIELD2 ASC]

                var fields = string.Empty;
                var condition = string.Empty;
                var sort = string.Empty;

                //fields
                if (queryContext.Fields == null)
                {
                    fields = "*";
                }
                else
                {
                    fields = string.Format("Id, {0}", String.Join(",", queryContext.Fields.Select(ctx => ctx.Field)));
                }

                var tableName1 = "TB1";
                var tableName2 = "TB2";
                var sqlString = string.Format("( {0} ) AS {1}", queryContext.Sql, tableName1);

                //condition
                if (queryContext.XQueries != null && queryContext.XQueries.Any())
                {
                    condition = SqlForXQuery(queryContext.XQueries);
                }

                //order
                if (queryContext.Sorts != null && queryContext.Sorts.Any())
                {
                    sort = SqlForSort(queryContext.Sorts);
                }
                else
                {
                    sort = string.Format("ORDER BY {0}", nameId);
                }

                //paging
                //            SELECT *  FROM
                //  (SELECT  ROW_NUMBER() OVER (ORDER BY Id ASC) as RowNumber,*
                //  FROM Session) AS Session
                //WHERE RowNumber BETWEEN 11 AND 20


                var mainSql = string.Empty;
                var paginationSql = string.Empty;

                var pagination = queryContext.Pagination;
                if (pagination == null || !pagination.SupportPaging)
                {
                    //mainSql = string.IsNullOrEmpty(condition) ? string.Format("SELECT {0} FROM {1} {2}", fields, tableName, sort) : string.Format("SELECT {0} FROM {1} WHERE {2} {3}", fields, tableName, condition, sort);
                    mainSql = string.Format("SELECT {0} FROM {1} {2} {3}", fields, sqlString, string.IsNullOrEmpty(condition) ? string.Empty : string.Format("WHERE {0}", condition), sort);
                }
                else
                {
                    if (pagination.PaginationType == PaginationType.Offset)
                    {
                        //1, 4 => 0 to 4
                        //2, 4 => 4 to 
                        //3, 4
                        var startIndex = (queryContext.Pagination.Page - 1) * queryContext.Pagination.Count;
                        var endIndex = startIndex + queryContext.Pagination.Count;

                        //SELECT * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY Id) AS Row FROM Session WHERE Id In (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)) As cols WHERE Row BETWEEN 10 AND 20
                        mainSql = string.Format("SELECT * FROM (SELECT {0}, ROW_NUMBER() OVER ({1}) AS Row FROM {2} {3}) As {4} WHERE Row BETWEEN ({5} + 1) AND {6}", fields, sort, sqlString, string.IsNullOrEmpty(condition) ? string.Empty : string.Format("WHERE {0}", condition), tableName2, startIndex, endIndex);
                        paginationSql = string.Format("SELECT COUNT({0}) AS Total FROM {1} {2}", nameId, sqlString, string.IsNullOrEmpty(condition) ? string.Empty : string.Format("WHERE {0}", condition));
                    }
                    else if (pagination.PaginationType == PaginationType.Id)
                    {
                        var sinceId = pagination.SinceId;
                        var maxId = pagination.MaxId;
                        var count = pagination.Count.Value;

                        //Id > sinceId AND Id <= maxId
                        var sinceSql = sinceId.HasValue ? string.Format("{0} > {1}", nameId, sinceId.Value) : string.Format("{0} > 0", nameId);
                        var maxSql = maxId.HasValue ? string.Format("{0} <= {1}", nameId, maxId.Value) : string.Empty;
                        var sinceMaxSql = string.Empty;
                        if (sinceId.HasValue && maxId.HasValue)
                        {
                            sinceMaxSql = string.Format("({0} AND {1})", sinceSql, maxSql);
                        }
                        else
                        {
                            sinceMaxSql = string.Format("({0})", sinceId.HasValue ? sinceSql :
                                maxId.HasValue ? maxSql : sinceSql);

                        }

                        condition = string.IsNullOrEmpty(condition) ? string.Format("WHERE {0}", sinceMaxSql) : string.Format("WHERE {0} AND {1}", condition, sinceMaxSql);
                        mainSql = string.Format("SELECT TOP {0} {1} FROM {2} {3} {4}", count, fields, sqlString, condition, sort);
                        //paginationSql = string.Format("SELECT MIN(Id) AS SinceId, MAX(Id) As MaxId FROM {0} {1} {2}", tableName, condition, sort);
                        paginationSql = string.Format("SELECT MIN({0}.{1}) AS SinceId, MAX({0}.{1}) As MaxId FROM ({2}) AS {0}", tableName2, nameId, mainSql);
                    }
                    else if (pagination.PaginationType == PaginationType.Cursor)
                    {
                        throw new NotImplementedException("Cursor paging is not yet supported.");
                    }
                }

                var sqlBuilder = new StringBuilder(mainSql);

                //includes
                if (queryContext.Includes != null)
                {
                    foreach (var includeContext in queryContext.Includes)
                    {
                        var entityName = includeContext.Entity;
                        var propertyInfo = typeof(T).GetProperty(entityName, BindingFlags.Public | BindingFlags.IgnoreCase | BindingFlags.Instance);

                        if (propertyInfo != null)
                        {
                            var childTableName = propertyInfo.PropertyType.Name;

                            //var indexOfFrom = mainSql.IndexOf("FROM", StringComparison.InvariantCultureIgnoreCase);
                            //var temp = mainSql.Substring(6, indexOfFrom - 6);

                            sqlBuilder.AppendLine();
                            //sqlBuilder.Append(string.Format("SELECT * FROM {0} WHERE Id In ({1})", childTableName, mainSql.Replace(fields, string.Format(" {0}Id ", entityName))));
                            sqlBuilder.Append(string.Format("SELECT * FROM {0} WHERE {1} In ({2})", childTableName, "Id", mainSql
                                .Replace(fields, string.Format(" {0}Id ", entityName))));

                            if (pagination == null || !pagination.SupportPaging)
                            {
                                sqlBuilder.Replace(sort, string.Empty);
                            }

                        }
                    }
                }

                sqlBuilder.AppendLine();
                sqlBuilder.Append(paginationSql);

                var sql = sqlBuilder.ToString();
                return sql;
                #endregion
            }
        }
        #endregion

        #region SqlForXQuery
        private string SqlForXQuery(IEnumerable<XQueryContext> contexts)
        {
            //{"and":{"field":"higher", "op":"=","value":4}},
            //{"or":{"field":"description", "op":"LIKE","value":"%tui bam%"}},
            //{"or":{"field":"lower", "op":">","value":100}},
            //]

            var sqlBuilder = new StringBuilder();
            int count = 0;
            foreach (var context in contexts)
            {
                var conditionQuery = string.IsNullOrEmpty(context.RawQuery) ? string.Format("({0} {1} {2})", context.Field, context.Operator, context.ValueAsString()) :
                    string.Format("({0})", context.RawQuery);

                sqlBuilder.Append(count == 0
                    ? conditionQuery : string.Format("{0} {1}", context.And ? " AND" : " OR", conditionQuery));

                count++;
            }

            var sql = sqlBuilder.ToString();
            return sql;
        }
        #endregion

        #region SqlForSort
        private string SqlForSort(IEnumerable<SortContext> contexts)
        {
            //{"and":{"field":"higher", "op":"=","value":4}},
            //{"or":{"field":"description", "op":"LIKE","value":"%tui bam%"}},
            //{"or":{"field":"lower", "op":">","value":100}},
            //]

            var sqlBuilder = new StringBuilder("ORDER BY");
            int count = 0;
            foreach (var context in contexts)
            {
                sqlBuilder.Append(string.Format(" {0} {1},", context.Field, context.IsAscending ? "ASC" : "DESC"));
                count++;
            }

            var sql = sqlBuilder.ToString().SubstringFromEnd(1);
            return sql;
        }
        #endregion

        #region SqlForQueryOne
        public string SqlForQueryOne<T, TIdentifier>(TIdentifier id, QueryContext queryContext)
            where T : IEntity<TIdentifier>
        {
            //SELECT [FIELDS] FROM [TABLENAME] WHERE Id = [Id]

            #region Check IdAsGuid
            var idValue = TypeDescriptorCache.GetTypeDescriptor<T>().IdAsGuid ? string.Format("'{0}'", id) : id.ToString();
            #endregion

            var fields = string.Empty;

            //fields
            if (queryContext.Fields == null)
            {
                fields = "*";
            }
            else
            {
                fields = string.Format("Id, {0}", String.Join(",", queryContext.Fields.Select(ctx => ctx.Field)));
            }

            //tableName
            var tableName = typeof(T).Name;

            var mainSql = string.Format("SELECT {0} FROM {1} Where Id = {2}", fields, tableName, idValue);
            var sqlBuilder = new StringBuilder(mainSql);

            //includes
            if (queryContext.Includes != null)
            {
                foreach (var includeContext in queryContext.Includes)
                {
                    var entityName = includeContext.Entity;
                    var propertyInfo = typeof(T).GetProperty(entityName, BindingFlags.Public | BindingFlags.IgnoreCase | BindingFlags.Instance);

                    if (propertyInfo != null)
                    {
                        var childTableName = propertyInfo.PropertyType.Name;

                        var indexOfFrom = mainSql.IndexOf("FROM", StringComparison.InvariantCultureIgnoreCase);
                        var temp = mainSql.Substring(6, indexOfFrom - 6);

                        sqlBuilder.AppendLine();
                        sqlBuilder.Append(string.Format("SELECT * FROM {0} WHERE Id In ({1})", childTableName, mainSql.Replace(temp, string.Format(" {0}Id ", entityName))));
                    }
                }
            }

            var sql = sqlBuilder.ToString();
            return sql;
        }
        #endregion

        #region Update
        public SqlResult SqlForUpdate<T, TIdentifier>(TIdentifier id, dynamic data)
            where T : IEntity<TIdentifier>
        {
            if (data is JObject)
            {
                return SqlForUpdateWith<T, TIdentifier>(id, (JObject)data);
            }
            else
            {
                throw new NotImplementedException();
            }

        }

        private SqlResult SqlForUpdateWith<T, TIdentifier>(TIdentifier id, JObject data)
            where T : IEntity<TIdentifier>
        {
            //tableName
            var tableName = typeof(T).Name;

            //properties
            var paramNames = data.Properties().Select(prop => prop.Name)
                                              .Where(paramName => !paramName.Equals("Id", StringComparison.OrdinalIgnoreCase))
                                              .Where(paramName => !paramName.Equals("ModifiedDate", StringComparison.OrdinalIgnoreCase) && !paramName.Equals("ModifiedAt", StringComparison.OrdinalIgnoreCase) && !paramName.Equals("CreatedDate", StringComparison.OrdinalIgnoreCase) && !paramName.Equals("CreatedAt", StringComparison.OrdinalIgnoreCase))
                                              .Where(paramName => typeof(T).GetProperty(paramName, BindingFlags.Public | BindingFlags.IgnoreCase | BindingFlags.Instance) != null)
                                              .Select(p => p + "= @" + p)
                                              .ToList();
            paramNames.Add("ModifiedDate= GETUTCDATE()");
            paramNames.Add("ModifiedAt= CAST(DATEDIFF(s, '1970-01-01 00:00:00', GETUTCDATE()) AS BIGINT) * 1000");

            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append(string.Format("UPDATE {0} SET {1} WHERE Id = @Id",
                tableName,
                string.Join(",", paramNames)));
            sqlBuilder.AppendLine();
            sqlBuilder.Append(string.Format("SELECT * FROM {0} WHERE Id = @Id", tableName));

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("Id", id);
            foreach (var property in data.Properties())
            {
                var propertyName = property.Name;

                var propertyInfo = typeof(T).GetProperty(propertyName, BindingFlags.Public | BindingFlags.IgnoreCase | BindingFlags.Instance);

                if (propertyInfo != null)
                {
                    dynamic value = property.Value.GetValue();
                    parameters.Add(property.Name, value);
                }
            }

            return new SqlResult() { Sql = sqlBuilder.ToString(), Parameters = parameters };
        }
        #endregion

        #region Create
        public SqlResult SqlForCreate<T>(dynamic data)
        {
            if (data is JObject)
            {
                return SqlForCreateWith<T>((JObject)data);
            }
            else
            {
                throw new NotImplementedException();
            }

        }

        private SqlResult SqlForCreateWith<T>(JObject data)
        {
            //tableName
            var tableName = typeof(T).Name;

            //properties
            var paramNames = data.Properties().Select(prop => prop.Name)
                                              .Where(paramName => !paramName.Equals("ModifiedDate", StringComparison.OrdinalIgnoreCase) && !paramName.Equals("ModifiedAt", StringComparison.OrdinalIgnoreCase) && !paramName.Equals("CreatedDate", StringComparison.OrdinalIgnoreCase) && !paramName.Equals("CreatedAt", StringComparison.OrdinalIgnoreCase))
                                              .ToList();
            paramNames.Add("CreatedDate");
            paramNames.Add("CreatedAt");
            paramNames.Add("ModifiedDate");
            paramNames.Add("ModifiedAt");

            var paramValues = data.Properties().Select(prop => prop.Name)
                                              .Where(paramName => !paramName.Equals("ModifiedDate", StringComparison.OrdinalIgnoreCase) && !paramName.Equals("ModifiedAt", StringComparison.OrdinalIgnoreCase) && !paramName.Equals("CreatedDate", StringComparison.OrdinalIgnoreCase) && !paramName.Equals("CreatedAt", StringComparison.OrdinalIgnoreCase))
                                              .Select(p => "@" + p)
                                              .ToList();
            paramValues.Add("GETUTCDATE()");
            paramValues.Add("CAST(DATEDIFF(s, '1970-01-01 00:00:00', GETUTCDATE()) AS BIGINT) * 1000");
            paramValues.Add("GETUTCDATE()");
            paramValues.Add("CAST(DATEDIFF(s, '1970-01-01 00:00:00', GETUTCDATE()) AS BIGINT) * 1000");

            #region Check IdAsGuid
            var generatedId = string.Empty;
            if (TypeDescriptorCache.GetTypeDescriptor<T>().IdAsGuid)
            {
                generatedId = string.Format("'{0}'", Guid.NewGuid().ToString());

                paramNames.Insert(0, "Id");
                paramValues.Insert(0, generatedId);
            }
            else
            {
                generatedId = "SCOPE_IDENTITY()";
            }
            #endregion

            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append(string.Format("INSERT INTO {0} ({1}) VALUES({2})",
                tableName,
                string.Join(",", paramNames),
                string.Join(",", paramValues)));
            sqlBuilder.AppendLine();
            sqlBuilder.Append(string.Format("SELECT * FROM {0} WHERE Id = {1}", tableName, generatedId));

            DynamicParameters parameters = new DynamicParameters();
            foreach (var property in data.Properties())
            {
                var propertyName = property.Name;

                var propertyInfo = typeof(T).GetProperty(propertyName, BindingFlags.Public | BindingFlags.IgnoreCase | BindingFlags.Instance);

                if (propertyInfo != null)
                {
                    dynamic value = property.Value.GetValue();
                    parameters.Add(property.Name, value);
                }
            }

            return new SqlResult() { Sql = sqlBuilder.ToString(), Parameters = parameters };
        }
        #endregion

        public ITypeDescriptorCache TypeDescriptorCache { get; set; }

        public SqlServerQueryBuilder(ITypeDescriptorCache typeDescriptorCache)
        {
            this.TypeDescriptorCache = typeDescriptorCache;
        }
    }
}
