﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Data.TypeCache;
using Caf.Core.Models;
using Dapper;

namespace Caf.Core.Data
{
    public interface IQueryBuilder
    {
        ITypeDescriptorCache TypeDescriptorCache { get; set; }

        string SqlForQueryAll<T>(QueryContext queryContext);
        string SqlForQueryOne<T, TIdentifier>(TIdentifier id, QueryContext queryContext)
            where T : IEntity<TIdentifier>;

        SqlResult SqlForUpdate<T, TIdentifier>(TIdentifier id, dynamic data)
            where T : IEntity<TIdentifier>;

        SqlResult SqlForCreate<T>(dynamic data);
    }
}
