﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace Caf.Core.Extensions
{
    public static class JTokenExtensions
    {
        public static dynamic GetValue(this JToken token)
        {
            dynamic result = null;

            if (token.Type == JTokenType.Integer)
            {
                result = token.Value<int>();
            }
            else if (token.Type == JTokenType.Boolean)
            {
                result = token.Value<bool>();
            }
            else if (token.Type == JTokenType.String)
            {
                result = token.Value<string>();
            }
            else if (token.Type == JTokenType.Date)
            {
                result = token.Value<string>();
            }
            else if (token.Type == JTokenType.Float)
            {
                result = token.Value<double>();
            }
            else if (token.Type == JTokenType.Guid)
            {
                var temp = token.Value<Guid>();
                result = temp.ToString();
            }
            else if (token.Type == JTokenType.Array || token.Type == JTokenType.Object)
            {
                //need to check for this, not working right now
                //criteria.Value = property.Value.Value<int[]>();
                //result = token.ToObject<StringCollection>().Serialized;
                result = JsonConvert.SerializeObject(token, new JsonSerializerSettings() { Formatting = Formatting.None, ContractResolver = new CamelCasePropertyNamesContractResolver() });
            }
            else if (token.Type == JTokenType.Null)
            {
                result = null;
            }
            else
            {
                throw new NotImplementedException();
            }

            return result;
        }
    }
}
