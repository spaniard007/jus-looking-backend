﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Caf.Core.Data;
using Caf.Core.Models;
using Caf.Web.WebApi.AttributesCustom;
using Caf.Web.WebApi.Infrastructures;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity.Owin;


namespace Caf.Web.WebApi.Controllers
{
    [Authorize]
    public class CrudApiControllerBase<TDataContext, TEntity, TIdentifier> : ApiControllerBase
        where TDataContext : DbContext, new()
        where TEntity : class, IEntity<TIdentifier>, IAudit
    {
        #region Constants
        public static string STRING_FORMAT_FOR_NOT_ENTITY_NOT_FOUND = "Cannot find entity with Id: {0}";
        public static string STRING_FORMAT_FOR_CANNOT_ADD_ENTITY = "Cannot add new entity.";
        public static string STRING_FORMAT_FOR_CANNOT_UPDATE_ENTITY = "Cannot update new entity.";
        public static string STRING_FORMAT_FOR_CANNOT_DELETE_ENTITY = "Cannot delete new entity.";
        #endregion

        #region Get All

        [HttpGet]
        [CustomRoute("", 1, "admin")]
        public virtual async Task<IHttpActionResult> GetAll([FromUri] QueryContextModel queryModel)
        {
            var context = this.Request.GetOwinContext().Get<TDataContext>();
            var queryContext = QueryContext.Build().FromQueryModel(queryModel);

            QueryResult queryResult = null;

            try
            {
                queryResult = this.QueryStore.Use(context).QueryAll<TEntity>(queryContext);
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }

            return this.AsSuccessResponse(queryResult);
        }

        #endregion

        #region Create
        [HttpPost]
        [CustomRoute("", 1, "admin")]
        public virtual async Task<IHttpActionResult> Create([FromBody] JObject data)
        {
            if (data == null) return this.AsFailedResponse("Input data is not valid", HttpStatusCode.BadRequest);

            CreateOrUpdateResult result = null;
            var context = this.Request.GetOwinContext().Get<TDataContext>();

            try
            {
                result = QueryStore.Use(context).Create<TEntity>(data);
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }

            var createdEntity = result.SingleResult;
            return this.AsSuccessResponse(createdEntity, HttpStatusCode.OK);
        }

        #endregion

        #region Get
        [HttpGet]
        [CustomRoute("{id}", 1, "admin")]
        public virtual async Task<IHttpActionResult> Get(TIdentifier id, [FromUri] QueryContextModel queryModel)
        {
            var context = this.Request.GetOwinContext().Get<TDataContext>();
            var queryContext = QueryContext.Build().FromQueryModel(queryModel);

            QueryResult queryResult = null;

            try
            {
                queryResult = this.QueryStore.Use(context).QueryOne<TEntity, TIdentifier>(id, queryContext);

                if (queryResult.SingleResult == null)
                {
                    return this.AsFailedResponse(string.Format(STRING_FORMAT_FOR_NOT_ENTITY_NOT_FOUND, id), HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }

            return this.AsSuccessResponse(queryResult);
        }

        #endregion

        #region PartialUpdate

        [HttpPatch]
        [CustomRoute("{id}", 1, "admin")]
        public virtual async Task<IHttpActionResult> PartialUpdate(TIdentifier id, [FromBody] JObject data)
        {
            CreateOrUpdateResult result = null;
            var context = this.Request.GetOwinContext().Get<TDataContext>();

            try
            {
                result = QueryStore.Use(context).Update<TEntity, TIdentifier>(id, data);
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }

            var updatedEntity = result.SingleResult;
            return this.AsSuccessResponse(updatedEntity, HttpStatusCode.OK);
        }
        #endregion

        #region Delete

        [HttpDelete]
        [CustomRoute("{id}", 1, "admin")]
        public virtual async Task<IHttpActionResult> Delete(TIdentifier id)
        {
            var context = this.Request.GetOwinContext().Get<TDataContext>();
            var entity = context.Set<TEntity>().Find(id);

            if (entity == null)
            {
                return this.AsFailedResponse(string.Format(STRING_FORMAT_FOR_NOT_ENTITY_NOT_FOUND, id),
                    HttpStatusCode.NotFound);
            }

            try
            {
                context.Set<TEntity>().Remove(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }

            return this.AsSuccessResponse(HttpStatusCode.OK);
        }

        #endregion

        #region From Entities
        protected virtual IList<dynamic> FromEntities(IList<TEntity> entities)
        {
            var dtos = new List<dynamic>();

            foreach (var entity in entities)
            {
                dtos.Add(FromEntity(entity));
            }

            return dtos;
        }
        #endregion

        #region From Entity

        protected virtual TEntity FromEntity(TEntity entity)
        {
            return entity;
        }

        #endregion
    }
}

