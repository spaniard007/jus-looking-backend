﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Caf.Core.Converters;
using Caf.Core.Data;
using Newtonsoft.Json;

namespace Caf.Web.WebApi.Controllers
{
    public class QueryContextModel
    {
        public string Sort { get; set; }
        public string Order { get; set; }

        public string Fields { get; set; }

        public string Search { get; set; }

        public string Includes { get; set; }

        public int? Page { get; set; }

        public int? Count { get; set; }

        #region MaxId
        public long? MaxId { get; set; }
        #endregion

        #region SinceId
        public long? SinceId { get; set; }
        #endregion 
    }

    public static class QueryContextExtensions
    {
        public static QueryContext FromQueryModel(this QueryContext queryContext, QueryContextModel queryModel)
        {
            if (queryModel != null)
            {
                //sort
                SortContext[] sortContext = null;
                var sort = queryModel.Sort ?? queryModel.Order;
                if (!string.IsNullOrEmpty(sort))
                {
                    var sortStrs = sort.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    sortContext = new SortContext[sortStrs.Length];

                    for (int i = 0; i < sortStrs.Length; i++)
                    {
                        var sortStr = sortStrs[i].Trim();

                        var isAscending = !sortStr.StartsWith("-");
                        var sortProperty = isAscending ? sortStr : sortStr.Substring(1);

                        sortContext[i] = new SortContext() { Field = sortProperty, IsAscending = isAscending };
                    }
                }
                queryContext.Sorts = sortContext;

                //fields
                IEnumerable<FieldContext> fieldsContext = null;
                var fields = queryModel.Fields;
                if (!string.IsNullOrEmpty(fields))
                {
                    fieldsContext = fields.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(f => new FieldContext() { Field = f });
                }
                queryContext.Fields = fieldsContext;

                //includes
                IEnumerable<IncludeContext> includesContext = null;
                var includes = queryModel.Includes;
                if (!string.IsNullOrEmpty(includes))
                {
                    includesContext = includes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(s => new IncludeContext() { Entity = s });
                }
                queryContext.Includes = includesContext;

                //search
                IEnumerable<XQueryContext> searchContext = null;
                var search = queryModel.Search;
                if (!string.IsNullOrEmpty(search))
                {
                    //have issue with search '%da%'
                    //searchContext = JsonConvert.DeserializeObject<IEnumerable<XQueryContext>>(HttpUtility.UrlDecode(search), new XQueryFieldCriteriaConverter());
                    searchContext = JsonConvert.DeserializeObject<IEnumerable<XQueryContext>>(search, new XQueryFieldCriteriaConverter());
                }
                queryContext.XQueries = searchContext;

                //paging
                PagingContext pagingContext = new PagingContext();
                pagingContext.Page = queryModel.Page;
                pagingContext.Count = queryModel.Count.HasValue ? queryModel.Count : 20;
                pagingContext.Count = queryModel.Count;
                pagingContext.MaxId = queryModel.MaxId;
                pagingContext.SinceId = queryModel.SinceId;
                queryContext.Pagination = pagingContext;
            }

            return queryContext;
        }
    }
}
