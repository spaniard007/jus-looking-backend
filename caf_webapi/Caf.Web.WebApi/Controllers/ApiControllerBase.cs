﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Caf.Core.Data;
using Caf.Web.WebApi.Infrastructures;
using Microsoft.AspNet.Identity;

namespace Caf.Web.WebApi.Controllers
{
    public abstract class ApiControllerBase : ApiController
    {
        #region Query Store
        private IQueryStore queryStore = null;

        protected IQueryStore QueryStore
        {
            get
            {
                if (queryStore == null)
                {
                    queryStore = new QueryStore();
                }

                return queryStore;
            }

            set { this.queryStore = value; }
        }
        #endregion

        #region AsSuccessResponse
        protected virtual IHttpActionResult AsSuccessResponse(ApiResponse apiResponse, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            return Content(statusCode, apiResponse);
        }

        protected virtual IHttpActionResult AsSuccessResponse(dynamic data, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            ApiResponse response = ApiResponse.Create(data);
            return this.AsSuccessResponse(response, statusCode);
        }

        protected virtual IHttpActionResult AsSuccessResponse(HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            ApiResponse response = ApiResponse.Create(null, null, null);
            return this.AsSuccessResponse(response, statusCode);
        }

        protected virtual IHttpActionResult AsSuccessResponse(QueryResult queryResult, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            ApiResponse response = ApiResponse.Create(queryResult.MultipleResults ?? queryResult.SingleResult, queryResult.PaginationMetada);
            response.Sql = queryResult.Sql;

            return this.AsSuccessResponse(response, statusCode);
        }

        protected virtual IHttpActionResult AsSuccessResponse(CreateOrUpdateResult queryResult, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            ApiResponse response = ApiResponse.Create(queryResult.SingleResult);
            response.Sql = queryResult.Sql;

            return this.AsSuccessResponse(response, statusCode);
        }
        #endregion

        #region AsFailedResponse
        protected virtual IHttpActionResult AsFailedResponse(ApiResponse apiResponse, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            return Content(statusCode, apiResponse);
        }

        protected virtual IHttpActionResult AsFailedResponse(ApiErrorMessage apiErrorMessage, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            ApiResponse response = ApiResponse.Create(apiErrorMessage);
            return this.AsFailedResponse(response, statusCode);
        }

        protected virtual IHttpActionResult AsFailedResponse(List<ApiErrorMessage> apiErrorMessages, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            ApiResponse response = ApiResponse.Create(apiErrorMessages);
            return this.AsFailedResponse(response, statusCode);
        }

        protected virtual IHttpActionResult AsFailedResponse(int errorCode, string erroMessage, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            ApiResponse response = ApiResponse.Create(new ApiErrorMessage() { ErrorCode = errorCode, ErrorMessage = erroMessage });
            return this.AsFailedResponse(response, statusCode);
        }

        public virtual IHttpActionResult AsFailedResponse(IdentityResult result, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            var errors = ApiErrorMessage.FromIdentityResult(result);
            ApiResponse response = ApiResponse.Create(errors);

            return this.AsFailedResponse(response, statusCode);
        }

        protected virtual IHttpActionResult AsFailedResponse(Exception ex, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            ApiResponse response = ApiResponse.Create(ex);
            return this.AsFailedResponse(response, statusCode);
        }
        
        protected virtual IHttpActionResult AsFailedResponse(string erroMessage, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            return this.AsFailedResponse(ApiErrorMessage.GENERIC_ERROR_CODE, erroMessage, statusCode);
        }
        #endregion
    }
}
