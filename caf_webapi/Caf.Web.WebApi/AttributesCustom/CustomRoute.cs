﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http.Routing;

namespace Caf.Web.WebApi.AttributesCustom
{
    public class CustomRoute : RouteFactoryAttribute
    {
        public CustomRoute(string template, int version, string shape)
            : base(template)
        {
            AllowedAccept = new AcceptHeader
            {
                Version = version,
                Client = shape
            };
        }
        public AcceptHeader AllowedAccept
        {
            get;
            private set;
        }
        public override IDictionary<string, object> Constraints
        {
            get
            {
                var constraints = new HttpRouteValueDictionary
                {
                    {"accept", new AcceptConstraint(AllowedAccept)}
                };
                return constraints;
            }
        }
    }

    public class AcceptConstraint : IHttpRouteConstraint
    {
        public const string AcceptHeaderName = "accept";

        //private static readonly AcceptHeader DefaultAccept = new AcceptHeader
        //{
        //    Version = 1,
        //    Client = "mobile"
        //};

        public AcceptConstraint(AcceptHeader allowedAccept)
        {
            AllowedAccept = allowedAccept;
        }

        public AcceptHeader AllowedAccept { get; private set; }

        public bool Match(HttpRequestMessage request, IHttpRoute route, string parameterName, IDictionary<string, object> values, HttpRouteDirection routeDirection)
        {
            if (routeDirection == HttpRouteDirection.UriResolution)
            {
                string acceptString = GetAcceptHeader(request);

                if (!string.IsNullOrEmpty(acceptString))
                {
                    var accept = ConvertHeaderAccept(acceptString);

                    if (accept.Equals(AllowedAccept))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private string GetAcceptHeader(HttpRequestMessage request)
        {
            string acceptAsString;
            IEnumerable<string> headerValues;
            if (request.Headers.TryGetValues(AcceptHeaderName, out headerValues) && headerValues.Count() == 1)
            {
                acceptAsString = headerValues.First();
            }
            else
            {
                return null;
            }

            return acceptAsString;
        }

        private AcceptHeader ConvertHeaderAccept(string headerString)
        {
            if (string.IsNullOrEmpty(headerString))
            {
                return null;
            }

            try
            {
                //format = "{0}/vnd.{1}.{2}-v{3}+{4}";
                //0 : application
                //1 : company.atoms
                //2 : client (mobile or admin)
                //3 : \d+ (version)
                //4 : charater+
                //Example: application/vnd.app.atoms.mobile-v1+json

                string input = headerString.Trim();
                var match = Regex.Match(input, @"(\w+)\/vnd.(\w+.\w+).(\w+)-v(\d+)\+\w+");
                
                var app = match.Groups[1].ToString();
                var company = match.Groups[2].ToString();
                var client = match.Groups[3].ToString();
                var version = match.Groups[4].ToString();

                var accept = new AcceptHeader();
                accept.Client = client;
                int vertionInt;
                accept.Version = int.TryParse(version, out vertionInt) ? vertionInt : 0;

                return accept;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    public class AcceptHeader
    {
        public int Version { get; set; }
        public string Client { get; set; }

        public override bool Equals(object obj)
        {
            var header = obj as AcceptHeader;
            if (header != null)
            {
                return this.Version == header.Version && string.Equals(this.Client, header.Client);
            }
            else
            {
                return base.Equals(obj);    
            }
        }
    }
}