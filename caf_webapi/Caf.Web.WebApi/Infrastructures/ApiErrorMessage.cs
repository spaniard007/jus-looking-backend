﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Caf.Web.WebApi.Infrastructures
{
    public class ApiErrorMessage
    {
        public const int GENERIC_ERROR_CODE = 9999;

        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }

        public static IList<ApiErrorMessage> FromIdentityResult(IdentityResult result)
        {
            var errors = new List<ApiErrorMessage>();

            foreach (var errorResult in result.Errors)
            {
                errors.Add(new ApiErrorMessage() { ErrorMessage = errorResult, ErrorCode = 9998 });
            }

            return errors;
        }
        public static ApiErrorMessage FromException(Exception exception)
        {
            return new ApiErrorMessage() { ErrorMessage = exception.Message, ErrorCode = GENERIC_ERROR_CODE };
        }
    }
}
