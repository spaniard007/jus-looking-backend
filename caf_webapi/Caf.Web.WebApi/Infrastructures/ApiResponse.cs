﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Data;

namespace Caf.Web.WebApi.Infrastructures
{
    public class ApiResponse
    {
        #region Data
        public dynamic Data { get; set; }
        #endregion

        #region Errors
        public IList<ApiErrorMessage> Errors { get; set; }
        #endregion

        #region Pagination
        public PaginationMetaData Pagination { get; set; }
        #endregion

        #region Sql
        public string Sql { get; set; }
        #endregion

        #region Create
        public static ApiResponse Create(dynamic data, IList<ApiErrorMessage> errors, PaginationMetaData paginationMetadata)
        {
            var response = new ApiResponse();
            response.Data = data;
            response.Errors = errors;
            response.Pagination = paginationMetadata;

            return response;
        }

        public static ApiResponse Create(dynamic data)
        {
            return Create(data, null, null);
        }

        public static ApiResponse Create(IList<ApiErrorMessage> errors)
        {
            return Create(null, errors, null);
        }

        public static ApiResponse Create(dynamic data, PaginationMetaData paginationMetadata)
        {
            return Create(data, null, paginationMetadata);
        }

        public static ApiResponse Create(ApiErrorMessage error)
        {
            return Create(null, new List<ApiErrorMessage>() { error }, null);
        }

        public static ApiResponse Create(Exception exception)
        {
            return Create(ApiErrorMessage.FromException(exception));
        }
        #endregion
    }
}
