﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Routing;
using avb.Core.Data;
using avb.Web;
using avb.Web.Hubs;
using avb.Web.Infrastructures;
using avb.Web.Providers;
using avb.Web.Services.EmailService;
using avb.Web.SwaggerExtensions;
using Dapper.Contrib.Extensions;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Owin;
using Swashbuckle.Application;

[assembly: OwinStartup(typeof(Startup))]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)]
namespace avb.Web
{
    public class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        public static string FacebookAppId = ConfigurationManager.AppSettings["Facebook.AppId"];
        public static string FacebookAppToken = ConfigurationManager.AppSettings["Facebook.AppToken"]; //Facebook AppToken You can get it from here: https://developers.facebook.com/tools/accesstoken/

        public static double TokenExipration = 365; //in days
        public static double TokenSessionExipration = 14; //in days


        public void Configuration(IAppBuilder app)
        {
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            var httpConfig = new HttpConfiguration();

            #region Swagger
            httpConfig
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "AVB APIs")
                        .Description(@"  
<h4>1. Oauth: </h4>
<br/> - After login, you use access token
<br/> - Add header request: <b> Authorization: Bearer [access token] </b>
<br/> <b>OR</b> </b>
<br/> - Add query string: <b> apiKey=[access token] </b>
<h4>2. Mode mobile & admin: </h4>
<br/> - Mobile header request: <b> Accept: application/vnd.app.avb.mobile-v1+json </b>
<br/> - Admin header request: <b> Accept: application/vnd.app.avb.admin-v1+json </b>
<h4>3. Example Query: </h4>
<br/> Object's fields: id, createdAt, creatorId, creator (this field is relationship)
<br/> - sort=-createdAt,id
<br/> - order=-createdAt,id
<br/> - fields=creatorId   (field Id is default)
<br/> - search=[{""and"":{""field"":""category"",""op"":""="",""value"":4}},{""or"":{""field"":""creatorId"",""op"":""="",""value"":""dce43746-52ce-4d16-be4d-b5df7095bf10""}}]
<br/> - includes=creator
<br/> - offset paging: set page + count   (support for web)
<br/> - id paging: set sinceId/maxId + count    (support for mobile)
");
                    c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                    c.OperationFilter<DescriptionSwaggerExtension>();
                    c.OperationFilter<AddFileUploadParams>();
                    c.OperationFilter<DescriptionSwaggerExtension>();
                    c.OperationFilter<CustomResponseType>();
                })
                .EnableSwaggerUi(c =>
                {
                    c.CustomAsset("index", typeof(Startup).Assembly, "avb.Web.swaggerui.index.html");
                });
            #endregion

            ConfigureOAuthTokenGeneration(app);

            #region SignalR
            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new SignalRContractResolver();
            var serializer = JsonSerializer.Create(settings);
            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => new ApplicationUserIdProvider());
                GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => serializer);
                map.RunSignalR();
            });

            #endregion

            ConfigureWebApi(httpConfig);

            app.UseWebApi(httpConfig);

            SqlMapperExtensions.TableNameMapper = (type) =>
            {
                // do something here to pluralize the name of the type
                return type.Name;
            };

        }

        private void ConfigureOAuthTokenGeneration(IAppBuilder app)
        {
            app.CreatePerOwinContext(DataContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>((options, context) => ApplicationUserManager.Create(options,context,new SendGridEmailService()));
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/api/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(TokenExipration),
                Provider = new ApplicationAuthorizationServerProvider()
            };

            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(
                new OAuthBearerAuthenticationOptions()
                {
                    Provider = new QueryStringOAuthBearerProvider()
                }
            );
        }

        private void ConfigureWebApi(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes(new CustomDirectRouteProvider());

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
            //config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeWithQualityHeaderValue("text/html"));

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonFormatter.SerializerSettings.Formatting = Formatting.Indented;
            jsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/octet-stream"));
        }
    }

    public class CustomDirectRouteProvider : DefaultDirectRouteProvider
    {
        protected override IReadOnlyList<IDirectRouteFactory>
        GetActionRouteFactories(HttpActionDescriptor actionDescriptor)
        {
            return actionDescriptor.GetCustomAttributes<IDirectRouteFactory>
            (inherit: true);
        }
    }
}