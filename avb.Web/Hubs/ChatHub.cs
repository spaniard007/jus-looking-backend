﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using avb.Core.Data;
using avb.Core.Models;
using avb.Web.Services.Logging;
using avb.Web.Services.PushNotification;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;

namespace avb.Web.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        private DataContext mDataContext = null;
        private LoggingService loggingService = null;
        private OneSignalService mOneSignalService = null;
        protected LoggingService LoggingService
        {
            get { return loggingService ?? (loggingService = new LogentriesLoggingService()); }
        }

        protected DataContext DataContext
        {
            get { return mDataContext ?? (mDataContext = new DataContext()); }
        }

        protected OneSignalService OneSignalService
        {
            get
            {
                return mOneSignalService ?? (mOneSignalService = new OneSignalService());
            }
        }
            
        public override Task OnConnected()
        {
            var userId = this.Context.User.Identity.GetUserId();
            var user = this.DataContext.Set<ApplicationUser>().FirstOrDefault(u => u.Id == userId);

            if (user != null)
            {
                user.Status = Status.Online;
                this.DataContext.Set<ApplicationUser>().AddOrUpdate(user);
                this.DataContext.SaveChanges();
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var userId = this.Context.User.Identity.GetUserId();
            var user = this.DataContext.Set<ApplicationUser>().FirstOrDefault(u => u.Id == userId);

            if (user != null)
            {
                user.Status = Status.Offline;
                this.DataContext.Set<ApplicationUser>().AddOrUpdate(user);
                this.DataContext.SaveChanges();
            }
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            var userId = this.Context.User.Identity.GetUserId();
            var user = this.DataContext.Set<ApplicationUser>().FirstOrDefault(u => u.Id == userId);

            if (user != null && user.Status == Status.Offline)
            {
                user.Status = Status.Offline;
                this.DataContext.Set<ApplicationUser>().AddOrUpdate(user);
                this.DataContext.SaveChanges();
            }
            return base.OnReconnected();
        }
        [Authorize]
        public MessageReturnModel SendMessage(string toUserId, string text, MessageType messageType = MessageType.Text)
        {
            try
            {
                var fromUserId = this.Context.User.Identity.GetUserId();

               

                var conversation =
                    this.DataContext.Set<Conversation>()
                        .FirstOrDefault(c => c.ToUserId == toUserId && c.FromUserId == fromUserId || c.ToUserId == fromUserId && c.FromUserId == toUserId);
                if (conversation != null)
                {
                    conversation.LastMessage = text;
                    conversation.LastMessageType = messageType;
                    this.DataContext.Set<Conversation>().AddOrUpdate(conversation);
                }
                else
                {
                    conversation = new Conversation()
                    {
                        FromUserId = fromUserId,
                        ToUserId = toUserId,
                        LastMessage = text,
                        LastMessageType = messageType
                    };
                    this.DataContext.Set<Conversation>().Add(conversation);
                }


                var message = new Message()
                {
                    Content = text,
                    MessageType = messageType,
                    ConversationId = conversation.Id,
                    OwnerId = fromUserId,
                    Status = MessageStatus.Sent
                };


                var user = this.DataContext.Set<ApplicationUser>().FirstOrDefault(u => u.Id == toUserId);
                if (user != null && user.Status == Status.Online)
                {
                    message.Status = MessageStatus.Seen;
                }
                
                this.DataContext.Set<Message>().Add(message);
                this.DataContext.SaveChanges();


                var messageReturnModel = MessageReturnModel.Create(message);

                Clients.User(toUserId).messageReceived(messageReturnModel);

                #region PushNotification if user offline
                
                if (user != null && user.Status == Status.Offline && !String.IsNullOrEmpty(user.PlayerId))
                {
                    var fromUser = this.DataContext.Set<ApplicationUser>().FirstOrDefault(acc => acc.Id == fromUserId);
                    var content = String.Format("{0} {1} has sent {2}.",
                        String.IsNullOrEmpty(fromUser.FirstName) ? "" : fromUser.FirstName,
                        String.IsNullOrEmpty(fromUser.LastName) ? "" : fromUser.LastName, messageType == MessageType.Text ? "a message." : "an image.");
                    // Push notification

                    var pushNotificationModel = PushNotificationModel.Create(message, fromUser,toUserId);
                    OneSignalService.PushNotificationForUsers(new string[] { user.PlayerId }, "AVB", content.Trim(),fromUserId, pushNotificationModel,
                        "");
                }
               
                #endregion

                return messageReturnModel;
            }
            catch (Exception ex)
            {
                this.LoggingService.Error(ex);

                return null;
            }
        }

        [Authorize]
        public MessageReturnModel UpdateMessage(Guid messageId, string toUserId)
        {
            try
            {
                Message message = this.DataContext.Set<Message>().FirstOrDefault(m => m.Id == messageId);
                if (message != null)
                {
                    message.Status = MessageStatus.Seen;
                }


                this.DataContext.Set<Message>().AddOrUpdate(message);
                this.DataContext.SaveChanges();


                var messageReturnModel = MessageReturnModel.Create(message);

                Clients.User(toUserId).statusReceived(messageReturnModel);

                var user = this.DataContext.Set<ApplicationUser>().FirstOrDefault(u => u.Id == toUserId);
                if (user != null && user.Status == Status.Offline)
                {
                    // Push notification
                }

                return messageReturnModel;
            }
            catch (Exception ex)
            {
                this.LoggingService.Error(ex);

                return null;
            }
        }
    }
    public class MessageReturnModel
    {

        public Guid Id { get; set; }
        public Guid ConversationId { get; set; }
        public string Content { get; set; }
        public MessageType MessageType { get; set; }

        public MessageStatus Status { get; set; }
        public DateTime CreatedDate { get; set; }

        public string OwnerId { get; set; }

        public static MessageReturnModel Create(Message message)
        {
            return new MessageReturnModel()
            {
                Id = message.Id,
                ConversationId = message.ConversationId,
                OwnerId = message.OwnerId,
                Content = message.Content,
                MessageType = message.MessageType,
                CreatedDate = message.CreatedDate,
                Status =  message.Status
            };
        }
    }

    public class PushNotificationModel
    {
        public Guid Id { get; set; }
        public Guid ConversationId { get; set; }
        public string Content { get; set; }
        public MessageType MessageType { get; set; }
        public MessageStatus Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public string OwnerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public RelationshipStatus RelationshipStatus { get; set; }
        public string Avatar { get; set; }

        public string OtherId { get; set; }

        public static PushNotificationModel Create(Message message, ApplicationUser fromUser,string toUserId)
        {
            return new PushNotificationModel()
            {
                Id = message.Id,
                ConversationId = message.ConversationId,
                OwnerId = message.OwnerId,
                Content = message.Content,
                MessageType = message.MessageType,
                CreatedDate = message.CreatedDate,
                Status = message.Status,
                FirstName = fromUser.FirstName,
                LastName = fromUser.LastName,
                RelationshipStatus = fromUser.RelationshipStatus,
                Avatar = fromUser.Avatar,
                OtherId =  toUserId
            };
        }
    }
}