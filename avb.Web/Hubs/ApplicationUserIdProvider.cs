﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;

namespace avb.Web.Hubs
{
    public class ApplicationUserIdProvider : IUserIdProvider
    {
        #region GetUserId
        public string GetUserId(IRequest request)
        {
            var userId = request.User.Identity.GetUserId();
            return userId;
        }
        #endregion
    }
}