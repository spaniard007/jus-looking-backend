﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using avb.Core.Data;
using avb.Web.Providers;
using Microsoft.AspNet.Identity.Owin;

namespace avb.Web.AttributesCustom
{
    public class SessionAuthorizeAttribute : AuthorizeAttribute
    {
        #region Fields
        private DataContext dataContext = null;

        protected DataContext DataContext
        {
            get { return this.dataContext ?? HttpContext.Current.GetOwinContext().Get<DataContext>(); }
        }
        #endregion

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (SkipAuthorization(actionContext))
            {
                return;
            }

            var userSessionManager = new UserSessionManager();
            if (userSessionManager.ReValidateSession())
            {
                base.OnAuthorization(actionContext);
            }
            else
            {
                actionContext.Response = actionContext.ControllerContext.Request.CreateErrorResponse(
                    HttpStatusCode.Unauthorized, "Session token expried or not valid.");
            }
        }

        private static bool SkipAuthorization(HttpActionContext actionContext)
        {
            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                   || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }
    }
}