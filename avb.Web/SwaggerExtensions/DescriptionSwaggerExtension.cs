﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using Caf.Web.WebApi.AttributesCustom;
using Swashbuckle.Swagger;

namespace avb.Web.SwaggerExtensions
{
    public class DescriptionSwaggerExtension : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            var attribute = apiDescription.ActionDescriptor
                .GetCustomAttributes<CustomRoute>().FirstOrDefault();

            if (attribute == null)
            {
                operation.summary = string.Format("Api for all");
            }
            else
            {
                operation.summary = string.Format("Api for {0}", attribute.AllowedAccept.Client);
            }
        }
    }
}