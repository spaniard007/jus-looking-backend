﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace avb.Web.SwaggerExtensions
{
    public class CustomResponseType : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            operation.produces.Clear();
            operation.produces.Add("application/vnd.app.avb.mobile-v1+json");
            operation.produces.Add("application/vnd.app.avb.admin-v1+json");
        }
    }
}