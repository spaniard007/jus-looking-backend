﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Caf.Web.WebApi.AttributesCustom;
using Swashbuckle.Application;
using Swashbuckle.Swagger;

namespace avb.Web.Controllers
{
    [RoutePrefix("")]
    public class HomeController : ApiController
    {
        private const string RESET_PASSWORD_DEEPLINK = "avb://resetPassword/";

        #region GET /deepLink

        [HttpGet]
        [AllowAnonymous]
        [Route("resetPassword/")]
        public async Task<IHttpActionResult> ResetPassword(string token)
        {
            return Redirect(RESET_PASSWORD_DEEPLINK + token);
        }


        #endregion
    }
}