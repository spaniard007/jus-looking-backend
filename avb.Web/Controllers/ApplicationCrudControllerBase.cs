﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using avb.Core.Data;
using avb.Web.Services.Logging;
using Caf.Core.Models;
using Caf.Web.WebApi.Controllers;
using Caf.Web.WebApi.Infrastructures;
using Microsoft.AspNet.Identity.Owin;

namespace avb.Web.Controllers
{
    public class ApplicationCrudControllerBase<TEntity> : CrudApiControllerBase<DataContext, TEntity, Guid>
        where TEntity : GuidEntityBase
    {
        #region Fields
        private DataContext dataContext = null;
        private LoggingService loggingService = null;

        protected DataContext DataContext
        {
            get { return this.dataContext ?? this.Request.GetOwinContext().Get<DataContext>(); }
        }

        protected LoggingService LoggingService
        {
            get { return loggingService ?? (loggingService = new LogentriesLoggingService()); }
        }
        #endregion

        protected override IHttpActionResult AsFailedResponse(ApiResponse apiResponse, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            this.LoggingService.Error(apiResponse);
            return base.AsFailedResponse(apiResponse, statusCode);
        }
    }
}