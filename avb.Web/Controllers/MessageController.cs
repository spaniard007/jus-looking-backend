﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using avb.Core.Models;
using avb.Web.AttributesCustom;
using avb.Web.Models.ReturnModels;
using Caf.Core.Data;
using Caf.Web.WebApi.AttributesCustom;
using Dapper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace avb.Web.Controllers
{
    [RoutePrefix("api/messages")]
    public class MessageController : ApplicationCrudControllerBase<Message>
    {
        #region GET /api/users/current/{conversationId:Guid}/messages
        [HttpGet]
        [CustomRoute("~/api/users/current/conversations/{conversationId:Guid}/messages", 1, "mobile")]
        [SessionAuthorize]
        public async Task<IHttpActionResult> GetMyMessagesInConversation(Guid conversationId, DateTime? sinceDateTime = null, DateTime? toDateTime = null, int? count = null, int? sinceId = null, int? maxId = null, int? page = null, string fields = null)
        {
            try
            {
                QueryResult queryResult = null;

                var userId = User.Identity.GetUserId();
                var messageTable = string.Empty;

                if (sinceDateTime != null)
                {
                    var dt = sinceDateTime.Value.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffK");
                    messageTable = string.Format(@" CreatedDate >= CONVERT(datetime,'{0}') ", dt);
                }

                if (toDateTime != null)
                {
                    var dt = toDateTime.Value.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffK");
                    messageTable = string.IsNullOrEmpty(messageTable)
                        ? string.Format(@" CreatedDate < CONVERT(datetime,'{0}') ", dt)
                        : string.Format(@" {0} AND CreatedDate < CONVERT(datetime,'{1}') ", messageTable, dt);;
                }

                messageTable = string.IsNullOrEmpty(messageTable) ? "Message" : string.Format(@"(SELECT * FROM Message WHERE {0}) Message", messageTable);

                var queryStringStore = string.Format(@"
SELECT Message.* FROM
	(
	SELECT Id,DeletedDateTimeFromer,DeletedDateTimeToer,ToUserId,FromUserId FROM Conversation 
	WHERE Id = '{0}'
	) Conversation
INNER JOIN {2}
ON Conversation.Id = Message.ConversationId
WHERE (Conversation.FromUserId = '{1}' AND Message.CreatedDate > Conversation.DeletedDateTimeFromer)
OR (Conversation.ToUserId= '{1}' AND Message.CreatedDate > Conversation.DeletedDateTimeToer)

", conversationId, userId, messageTable);
                // Update message
                var updateMessage = String.Format(@"UPDATE Message
SET Status = 2
WHERE ConversationId = '{0}' AND OwnerId != '{1}' AND Status !=  2", conversationId, userId);
                await this.DataContext.Database.Connection.ExecuteAsync(updateMessage);
                //Get message
                var queryContext = QueryContext.Build()
                    .WithSql(queryStringStore)
                    .WithFields(fields)
                    .WithPaging(count, sinceId, maxId, page)
                    .Sort("createdAt", false);

                queryResult = this.QueryStore.Use(this.DataContext).QueryAll<dynamic>(queryContext);


                return this.AsSuccessResponse(queryResult);
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }
        }
        #endregion
    }

}