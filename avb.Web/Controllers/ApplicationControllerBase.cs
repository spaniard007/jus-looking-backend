﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using avb.Core.Data;
using avb.Web.Infrastructures;
using avb.Web.Services.Logging;
using Caf.Web.WebApi.Controllers;
using Caf.Web.WebApi.Infrastructures;
using Microsoft.AspNet.Identity.Owin;

namespace avb.Web.Controllers
{
    public abstract class ApplicationControllerBase : ApiControllerBase
    {
        #region Fields
        private DataContext dataContext = null;
        private ApplicationUserManager applicationUserManager = null;
        private ApplicationRoleManager applicationRoleManager = null;
        private LoggingService loggingService = null;

        protected DataContext DataContext
        {
            get { return this.dataContext ?? this.Request.GetOwinContext().Get<DataContext>(); }
        }

        protected ApplicationUserManager ApplicationUserManager
        {
            get { return this.applicationUserManager ?? this.Request.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }

        protected ApplicationRoleManager ApplicationRoleManager
        {
            get { return this.applicationRoleManager ?? this.Request.GetOwinContext().GetUserManager<ApplicationRoleManager>(); }
        }

        protected LoggingService LoggingService
        {
            get { return loggingService ?? (loggingService = new LogentriesLoggingService()); }
        }
        #endregion

        protected override IHttpActionResult AsFailedResponse(ApiResponse apiResponse, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            this.LoggingService.Error(apiResponse);
            return base.AsFailedResponse(apiResponse, statusCode);
        }
    }
}