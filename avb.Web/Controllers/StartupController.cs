﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SqlTypes;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using avb.Core.Data;
using avb.Core.Extensions;
using avb.Core.Models;
using avb.Web.Services.MapService;
using avb.Web.Services.PushNotification;
using Caf.Core.Data;
using Dapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace avb.Web.Controllers
{
    [RoutePrefix("api")]
    public class StartupController : ApplicationControllerBase
    {
        private const string SERVER_FORMAT = "{0} SERVER ON {1}";


        private IMapService mMapService;
        protected IMapService MapService
        {
            get
            {
                return mMapService ?? (mMapService = new GoogleMapService());
            }
        }

        #region GET /api
        [HttpGet]
        [Route]
        public IHttpActionResult PingServer()
        {
            var serverContent = "EMPTY";

#if staging
            serverContent = string.Format(SERVER_FORMAT, "staging", DateTime.UtcNow);
#elif production
            serverContent = string.Format(SERVER_FORMAT, "production", DateTime.UtcNow);
#else
            serverContent = string.Format(SERVER_FORMAT, "dev", DateTime.UtcNow);
#endif
            return this.AsSuccessResponse(serverContent, HttpStatusCode.OK);
        }
        #endregion

        #region GET /api/database
        [HttpGet]
        [Route("database")]
        public IHttpActionResult SetupDatabase()
        {
            try
            {
                var dataContext = Request.GetOwinContext().Get<DataContext>();
                var teams = dataContext.Set<ApplicationUser>().ToList();
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }

            return this.AsSuccessResponse("API and Database ready at " + DateTime.UtcNow);
        }
        #endregion

        #region GET /api/testsecure
        [HttpGet]
        [Authorize]
        [Route("testsecure")]
        public IHttpActionResult TestSecure()
        {
            var userId = this.User.Identity.GetUserId();
            return this.AsSuccessResponse(userId);
        }
        #endregion

        #region GET /api/getUsers
        [HttpGet]
        [Authorize]
        [Route("getUsers")]
        public IHttpActionResult GetUsers()
        {
            var queryString = @"SELECT Id,Email FROM ApplicationUser";

            var queryResult = this.DataContext.Database.Connection.Query<dynamic>(queryString);

            return this.AsSuccessResponse(queryResult);
        }
        #endregion

        #region GET /api/sendEmail
        [HttpGet]
        [Authorize]
        [Route("sendEmail")]
        public async Task<IHttpActionResult> SendEmail()
        {
            var user = await this.ApplicationUserManager.FindByNameAsync("minh.luu@beesightsoft.com");
            if (user != null)
            {
                await this.ApplicationUserManager.SendEmailAsync(user.Id, "AVB test", "AVB test");
                return this.AsSuccessResponse("Success");

            }
            return this.AsFailedResponse("Fail");
        }
        #endregion

        #region GET /api/testPush
        [HttpGet]
        [AllowAnonymous]
        [Route("testPush")]
        public async Task<IHttpActionResult> PushNotification(string playerId)
        {
            var service = new OneSignalService();
            await service.PushNotificationForUsers(new string[] { playerId }, "AVB", "AVB has sent a message.", "message", new { data = "test" }, "");
            return this.AsSuccessResponse("success");
        }
        #endregion

        #region GET /api/updateClub

        [HttpGet]
        [AllowAnonymous]
        [Route("updateClub")]
        public async Task<IHttpActionResult> UpdateClub()
        {
            return this.AsSuccessResponse("success");
        }

        #endregion

        #region GET /api/createTestUsers

        [HttpGet]
        [AllowAnonymous]
        [Route("createTestUsers")]
        public async Task<IHttpActionResult> CreateTestUsers()
        {

            for (int i = 0; i < 100; i++)
            {
                await CreateRandomUser(i + 1);
            }

            return this.AsSuccessResponse("success");
        }

        #endregion

        #region GET /api/checkInAtLatLng
        [HttpGet]
        [AllowAnonymous]
        [Route("checkInAtLatLng")]
        public async Task<IHttpActionResult> CheckInAtLatLng(double lat, double lng, double? radius = 30000)
        {
            try
            {
                var result = await this.MapService.GetClubsNearBy(lat, lng, radius.Value, "", "");
                if (result.Status.Equals("OK"))
                {
                    // Add or Update Club
                    foreach (var place in result.Places)
                    {
                        var club = this.DataContext.Set<Club>().FirstOrDefault(c => c.Id == place.Id);
                        if (club == null)
                        {
                            club = new Club()
                        {
                            Id = place.Id,
                            Name = place.Name
                        };
                            this.DataContext.Set<Club>().AddOrUpdate(club);
                        }
                        else
                        {
                            club.Name = place.Name;
                        }

                    }
                    // Get 100 user
                    var queryUsers = @"SELECT TOP 100 Id FROM ApplicationUser";

                    var users = await this.DataContext.Database.Connection.QueryAsync(queryUsers);

                    foreach (var user in users)
                    {
                        string userId = user.Id;
                        // Get random club
                        var r = new Random();
                        var place = result.Places.ElementAt(r.Next(0, result.Places.Count));


                        #region Do Deactive the current check-in

                        string deactiveQuery = String.Format(@"
UPDATE CheckIn SET IsDelete = 1
WHERE UserCheckedInId = '{0}' AND IsDelete = 0", user.Id);

                        await this.DataContext.Database.Connection.ExecuteAsync(deactiveQuery);

                        #endregion

                        var checkIn = await
                       this.DataContext.Set<CheckIn>()
                           .FirstOrDefaultAsync(c => c.ClubId == place.Id && c.UserCheckedInId == userId);

                        if (checkIn == null)
                        {
                            checkIn = new CheckIn()
                            {
                                ClubId = place.Id,
                                UserCheckedInId = userId
                            };
                        }
                        else
                        {
                            checkIn.ModifiedDate = new SqlDateTime(DateTime.UtcNow).Value;
                            checkIn.IsDelete = false;
                        }
                        this.DataContext.Set<CheckIn>().AddOrUpdate(checkIn);
                    }
                    this.DataContext.SaveChanges();
                    return this.AsSuccessResponse("Success");
                }

                return this.AsFailedResponse("No club found.");
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }
        }
        #endregion

        #region Methods

        public async Task<ApplicationUser> CreateRandomUser(int index)
        {
            var tmp = GetUniqueKey(8);
            int type = index % 3 + 1;
            if (type == 3) type++;

            var applicationUser = new ApplicationUser
            {
                UserName = tmp,
                FirstName = "Person",
                LastName = index.ToString(),
                Email = tmp + "@gmail.com",
                RelationshipStatus = (RelationshipStatus)type
            };

            var createUserResult = await ApplicationUserManager.CreateAsync(applicationUser, "123456");
            if (createUserResult.Succeeded)
            {
                return applicationUser;
            }
            return null;
        }


        public string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
        #endregion
    }
}