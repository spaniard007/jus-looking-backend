﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SqlTypes;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Atoms.Web.Models.BindingModels;
using avb.Core.Extensions;
using avb.Core.Helper;
using avb.Core.Models;
using avb.Web.AttributesCustom;
using avb.Web.Constants;
using avb.Web.Infrastructures;
using avb.Web.Models.BindingModels;
using avb.Web.Models.ReturnModels;
using avb.Web.Providers;
using avb.Web.Services.PushNotification;
using Caf.Core.Data;
using Caf.Core.Data.TypeCache;
using Caf.Core.Extensions;
using Caf.Core.Models;
using Caf.Web.WebApi.AttributesCustom;
using Dapper;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace avb.Web.Controllers
{
    [RoutePrefix("api/users")]
    public class AccountController : ApplicationControllerBase
    {
        private IExternalOAuthProviderManager externalOAuthProviderManager;
        private OneSignalService mOneSignalService = null;
        protected OneSignalService OneSignalService
        {
            get
            {
                return mOneSignalService ?? (mOneSignalService = new OneSignalService());
            }
        }

        #region Constructors
        public AccountController()
        {
            this.externalOAuthProviderManager = new ExternalOAuthProviderManager(
                new FacebookOAuthProvider(Startup.FacebookAppId, Startup.FacebookAppToken)
                );
            //this.connectionManager = InMemoryConnectionManager.Instance;
        }
        #endregion

        #region POST /api/users/register
        [HttpPost]
        [AllowAnonymous]
        [CustomRoute("register", 1, "mobile")]
        public async Task<IHttpActionResult> Register(RegisterUserBindingModel registerUserBindingModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return this.AsFailedResponse(Converters.ModelStateToApiErrorConverter(ModelState), HttpStatusCode.BadRequest);
                }

                var applicationUser = new ApplicationUser
                {
                    UserName = registerUserBindingModel.Email,
                    Email = registerUserBindingModel.Email,
                    RelationshipStatus = RelationshipStatus.Undefined
                };

                var createUserResult = await ApplicationUserManager.CreateAsync(applicationUser, registerUserBindingModel.Password);

                if (!createUserResult.Succeeded)
                {
                    return this.AsFailedResponse(createUserResult);
                }

                return this.AsSuccessResponse("Created", HttpStatusCode.Created);
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region POST /api/users/login
        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<IHttpActionResult> Login(LoginBindingModel loginBindingModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return this.AsFailedResponse(Converters.ModelStateToApiErrorConverter(ModelState), HttpStatusCode.BadRequest);
                }

                ApplicationUser user = null;
                user = await ApplicationUserManager.FindAsync(loginBindingModel.UserName, loginBindingModel.Password);
                if (user == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2003), HttpStatusCode.BadRequest);
                }

                //if (user.IsDelete)
                //{
                //    return this.AsFailedResponse(ErrorsDefine.Find(2004));
                //}

                //generate access token response
                var accessTokenResponse = GenerateLocalAccessTokenResponse(user);

                #region Update UserSession
                var userSessionManager = new UserSessionManager();
                userSessionManager.CreateUserSession(user.Id, accessTokenResponse.Access_token);

                // Cleanup: delete expired sessions fromthe database
                userSessionManager.DeleteExpiredSessions();
                #endregion


                return this.AsSuccessResponse(accessTokenResponse);
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region POST /api/users/registerOrLoginExternal
        [HttpPost]
        [AllowAnonymous]
        [Route("registerOrLoginExternal")]
        public async Task<IHttpActionResult> RegisterOrLoginExternal(RegisterExternalBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return this.AsFailedResponse(Converters.ModelStateToApiErrorConverter(ModelState), HttpStatusCode.BadRequest);
                }

                var externalInfo = await externalOAuthProviderManager.VerifyExternalAccessToken(model.Provider, model.ExternalAccessToken);
                if (externalInfo == null)
                {
                    return this.AsFailedResponse("Invalid Provider or External Access Token", HttpStatusCode.BadRequest);
                }

                ApplicationUser user = null;

                user = await ApplicationUserManager.FindAsync(new UserLoginInfo(model.Provider, externalInfo.UserId));
                if (user == null)
                {
                    user = new ApplicationUser()
                    {
                        UserName = externalInfo.UserId,
                        Email = externalInfo.Email,
                        FirstName = externalInfo.FirstName,
                        LastName = externalInfo.LastName,
                        Gender = externalInfo.Gender,
                        Avatar = externalInfo.Avatar,
                        RelationshipStatus = RelationshipStatus.Undefined
                    };

                    var resultCreateUser = await ApplicationUserManager.CreateAsync(user);
                    if (!resultCreateUser.Succeeded)
                    {
                        return this.AsFailedResponse(resultCreateUser, HttpStatusCode.BadRequest);
                    }

                    //var resultAddRole = await ApplicationUserManager.AddToRoleAsync(user.Id, "User");
                    //if (!resultAddRole.Succeeded)
                    //{
                    //    return this.AsFailedResponse(resultAddRole, HttpStatusCode.BadRequest);
                    //}

                    var resultAddLogin = await ApplicationUserManager.AddLoginAsync(user.Id, new UserLoginInfo(model.Provider, externalInfo.UserId));
                    if (!resultAddLogin.Succeeded)
                    {
                        return this.AsFailedResponse(resultAddLogin, HttpStatusCode.BadRequest);
                    }
                }

                //if (user.IsDelete)
                //{
                //    return this.AsFailedResponse(ErrorsDefine.Find(2004), HttpStatusCode.BadRequest);
                //}


                //generate access token response
                var accessTokenResponse = GenerateLocalAccessTokenResponse(user);

                #region Update UserSession
                var userSessionManager = new UserSessionManager();
                userSessionManager.CreateUserSession(user.Id, accessTokenResponse.Access_token);

                // Cleanup: delete expired sessions fromthe database
                userSessionManager.DeleteExpiredSessions();
                #endregion

                return this.AsSuccessResponse(accessTokenResponse);
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region POST /api/users/resetPassword

        [HttpPost]
        [AllowAnonymous]
        [CustomRoute("resetPassword", 1, "mobile")]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return this.AsFailedResponse(Converters.ModelStateToApiErrorConverter(ModelState), HttpStatusCode.BadRequest);
                }

                ApplicationUser user = null;
                user = await ApplicationUserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2003), HttpStatusCode.BadRequest);
                }

                var token = await this.ApplicationUserManager.GeneratePasswordResetTokenAsync(user.Id);

                var response = new { resettoken = token, userid = user.Id };

                //Send reset password mail

                var resetToken = Encryption.Encrypt(JsonConvert.SerializeObject(response));

                var url = String.Format("{0}/resetPassword/?token={1}", Request.RequestUri.GetLeftPart(UriPartial.Authority), resetToken);

                var body = string.Format(@"
                                                <html>
                                                <body>
                                                You sent a request to reset your password <br/>
                                                Please click <a href='{0}'> here</a>.
                                                </body>
                                                </html>", url);


                await this.ApplicationUserManager.SendEmailAsync(user.Id, "AVB Reset Password", body);

                return this.AsSuccessResponse("Success");
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region POST /api/users/changePassword

        [HttpPost]
        [AllowAnonymous]
        [CustomRoute("changePassword", 1, "mobile")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return this.AsFailedResponse(Converters.ModelStateToApiErrorConverter(ModelState), HttpStatusCode.BadRequest);
                }
                var data = JsonConvert.DeserializeObject<dynamic>(Encryption.Decrypt(model.Token));
                var userId = data["userid"].ToString();
                var token = data["resettoken"].ToString();
                var result = await this.ApplicationUserManager.ResetPasswordAsync(userId, token, model.NewPassword);
                if (result.Succeeded)
                {
                    return this.AsSuccessResponse("Changed");
                }
                else
                {
                    return this.AsFailedResponse(result);
                }

            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region POST /api/users/logout
        [HttpPost]
        [SessionAuthorize]
        [CustomRoute("logout", 1, "mobile")]
        public async Task<IHttpActionResult> Logout()
        {
            var userId = this.User.Identity.GetUserId();

            var applicationUser = await this.ApplicationUserManager.FindByIdAsync(userId);

            if (applicationUser == null)
            {
                return this.AsFailedResponse(ErrorsDefine.Find(2003));
            }

            applicationUser.PlayerId = null;

            var result = await this.ApplicationUserManager.UpdateAsync(applicationUser);
            if (!result.Succeeded)
            {
                return this.AsFailedResponse(result, HttpStatusCode.BadRequest);
            }

            var userSessionManager = new UserSessionManager();
            userSessionManager.InvalidateUserSession();

            return this.AsSuccessResponse("Logout successfull");
        }

        #endregion

        #region GET /api/users/current
        [HttpGet]
        [SessionAuthorize]
        [CustomRoute("current", 1, "mobile")]
        public async Task<IHttpActionResult> GetProfile()
        {
            try
            {
                var userId = this.User.Identity.GetUserId();

                var applicationUser = await this.ApplicationUserManager.FindByIdAsync(userId);
                if (applicationUser == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2003));
                }

                return this.AsSuccessResponse(UserReturnModel.Create(applicationUser));
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }
        }

        #endregion

        #region POST /api/users/current
        [HttpPost]
        [SessionAuthorize]
        [CustomRoute("current", 1, "mobile")]
        public async Task<IHttpActionResult> NotifyToUser(NofityModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return this.AsFailedResponse(Converters.ModelStateToApiErrorConverter(ModelState), HttpStatusCode.BadRequest);
                }
                var user = await this.ApplicationUserManager.FindByIdAsync(model.UserId);
                if (user == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2005), HttpStatusCode.NotFound);
                }

                await
                    OneSignalService.PushNotificationForUsers(new string[] {user.PlayerId}, "AVB", "UpdateStatus", "",
                        null, "");

                return this.AsSuccessResponse("Notified");
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }
        }
        #endregion

        #region GET /api/users/{userId:Guid}
        [HttpGet]
        [SessionAuthorize]
        [CustomRoute("{userId:Guid}", 1, "mobile")]
        public async Task<IHttpActionResult> GetUserProfile(Guid userId)
        {
            try
            {
                var applicationUser = await this.ApplicationUserManager.FindByIdAsync(userId.ToString());
                if (applicationUser == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2003), HttpStatusCode.NotFound);
                }
                var currentUserId = this.User.Identity.GetUserId();

                var conversation =
                     this.DataContext.Set<Conversation>()
                         .FirstOrDefault(c => c.ToUserId == currentUserId && c.FromUserId == userId.ToString() || c.ToUserId == userId.ToString() && c.FromUserId == currentUserId);

                return this.AsSuccessResponse(UserReturnModel.Create(applicationUser, conversation == null ? (Guid?)null : conversation.Id));
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }
        }

        #endregion

        #region PATCH /api/users/current

        [HttpPut]
        [SessionAuthorize]
        [CustomRoute("current", 1, "mobile")]
        public async Task<IHttpActionResult> UpdateCurrentUserPut(UpdateProfileBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return this.AsFailedResponse(Converters.ModelStateToApiErrorConverter(ModelState), HttpStatusCode.BadRequest);
                }

                var userId = this.User.Identity.GetUserId();

                var applicationUser = await this.ApplicationUserManager.FindByIdAsync(userId);

                if (applicationUser == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2003));
                }

                applicationUser = Helper.FromDynamic(applicationUser, JObject.FromObject(model));

                var result = await this.ApplicationUserManager.UpdateAsync(applicationUser);
                if (!result.Succeeded)
                {
                    return this.AsFailedResponse(result, HttpStatusCode.BadRequest);
                }

                return this.AsSuccessResponse(UserReturnModel.Create(applicationUser));
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }
        }

        [HttpPatch]
        [SessionAuthorize]
        [CustomRoute("current", 1, "mobile")]
        public async Task<IHttpActionResult> UpdateCurrentUserPatch(dynamic model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return this.AsFailedResponse(Converters.ModelStateToApiErrorConverter(ModelState), HttpStatusCode.BadRequest);
                }

                var userId = this.User.Identity.GetUserId();

                var applicationUser = await this.ApplicationUserManager.FindByIdAsync(userId);



                if (applicationUser == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2003));
                }
                applicationUser = Helper.FromDynamic(applicationUser, model);
                //applicationUser.FromDynamic(model);

                //applicationUser.FirstName = model.FirstName;
                //applicationUser.LastName = model.LastName;
                //applicationUser.Gender = model.Gender;
                //applicationUser.Avatar = model.Avatar;
                //applicationUser.RelationshipStatus = model.RelationshipStatus;
                //applicationUser.Hashtags = model.Hashtags;
                //applicationUser.Bio = model.Bio;
                //applicationUser.DOB = model.DOB;
                //applicationUser.Photos = model.Photos;
                var result = await this.ApplicationUserManager.UpdateAsync(applicationUser);
                if (!result.Succeeded)
                {
                    return this.AsFailedResponse(result, HttpStatusCode.BadRequest);
                }

                return this.AsSuccessResponse(UserReturnModel.Create(applicationUser));
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }
        }

        #endregion

        #region POST /api/users/checkIn
        [HttpPost]
        [SessionAuthorize]
        [CustomRoute("checkIn", 1, "mobile")]
        public async Task<IHttpActionResult> CheckIn(CheckInBindingModel model)
        {
            try
            {
                if (!this.ModelState.IsValid)
                {
                    return this.AsFailedResponse(Converters.ModelStateToApiErrorConverter(ModelState), HttpStatusCode.BadRequest);
                }

                var userId = this.User.Identity.GetUserId();

                var applicationUser = await this.ApplicationUserManager.FindByIdAsync(userId);

                if (applicationUser == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2003), HttpStatusCode.NotFound);
                }

                var club = await this.DataContext.Set<Club>().FirstOrDefaultAsync(c => c.Id == model.ClubId);

                if (club == null)
                {
                    club = new Club()
                    {
                        Id = model.ClubId,
                        Name = model.Name
                    };
                     this.DataContext.Set<Club>().AddOrUpdate(club);
                }
                else
                {
                    club.Name = model.Name;
                }


                #region Do Deactive the current check-in

                var deactiveQuery = String.Format(@"
UPDATE CheckIn SET IsDelete = 1
WHERE UserCheckedInId = '{0}' AND IsDelete = 0", applicationUser.Id);

                await this.DataContext.Database.Connection.ExecuteAsync(deactiveQuery);

                #endregion

                var checkIn =
                    await
                        this.DataContext.Set<CheckIn>()
                            .FirstOrDefaultAsync(c => c.ClubId == club.Id && c.UserCheckedInId == applicationUser.Id);

                if (checkIn == null)
                {
                    checkIn = new CheckIn()
                    {
                        ClubId = club.Id,
                        UserCheckedInId = applicationUser.Id
                    };
                }
                else
                {
                    checkIn.ModifiedDate = new SqlDateTime(DateTime.UtcNow).Value;
                    checkIn.IsDelete = false;
                }

                this.DataContext.Set<CheckIn>().AddOrUpdate(checkIn);

                this.DataContext.SaveChanges();

                return this.AsSuccessResponse("Checked-in");
            }
            catch (Exception ex)
            {

                return this.AsFailedResponse(ex);
            }

        }

        #endregion

        #region GET /api/users/current/checkIns
        [HttpGet]
        [SessionAuthorize]
        [CustomRoute("current/checkIns", 1, "mobile")]
        public async Task<IHttpActionResult> GetCurrentCheckIns(int? count = null, int? sinceId = null, int? maxId = null, int? page = null)
        {
            try
            {

                var userId = this.User.Identity.GetUserId();

                var queryStringStore = String.Format(@"
SELECT Club.*,CheckIn.ModifiedDate,CheckIn.Id,CheckIn.SecondaryId FROM
(
	SELECT CheckIn.Id,CheckIn.ModifiedDate,CheckIn.ClubId, ROW_NUMBER() OVER (Order by ModifiedDate) AS SecondaryId FROM CheckIn WHERE DATEDIFF(SECOND,ModifiedDate,GETUTCDATE()) < {0} AND IsDelete = 0 AND CheckIn.UserCheckedInId = '{1}'
) CheckIn
INNER JOIN (SELECT Id AS ClubId,Name FROM Club) Club
ON CheckIn.ClubId = Club.ClubId", Constants.Constants.CheckInExpiration, userId);


                var queryContext = QueryContext.Build()
                  .WithSql(queryStringStore)
                  .WithPaging(count, sinceId, maxId, page)
                  .Sort("modifiedDate", false);

                var queryResult = this.QueryStore.Use(this.DataContext).QueryAll<dynamic>(queryContext);

                return this.AsSuccessResponse(queryResult);

            }
            catch (Exception ex)
            {

                return this.AsFailedResponse(ex);
            }

        }

        #endregion

        #region GET /api/users/{userId:Guid}/checkIns
        [HttpGet]
        [SessionAuthorize]
        [CustomRoute("{userId:Guid}/checkIns", 1, "mobile")]
        public async Task<IHttpActionResult> GetUserCheckIns(Guid userId,int? count = null, int? sinceId = null, int? maxId = null, int? page = null)
        {
            try
            {
                var queryStringStore = String.Format(@"
SELECT Club.*,CheckIn.ModifiedDate,CheckIn.Id,CheckIn.SecondaryId FROM
(
	SELECT CheckIn.Id,CheckIn.ModifiedDate,CheckIn.ClubId, ROW_NUMBER() OVER (Order by ModifiedDate) AS SecondaryId FROM CheckIn WHERE DATEDIFF(SECOND,ModifiedDate,GETUTCDATE()) < {0} AND IsDelete = 0 AND CheckIn.UserCheckedInId = '{1}'
) CheckIn
INNER JOIN (SELECT Id AS ClubId,Name FROM Club) Club
ON CheckIn.ClubId = Club.ClubId", Constants.Constants.CheckInExpiration, userId);


                var queryContext = QueryContext.Build()
                  .WithSql(queryStringStore)
                  .WithPaging(count, sinceId, maxId, page)
                  .Sort("modifiedDate", false);

                var queryResult = this.QueryStore.Use(this.DataContext).QueryAll<dynamic>(queryContext);

                return this.AsSuccessResponse(queryResult);

            }
            catch (Exception ex)
            {

                return this.AsFailedResponse(ex);
            }

        }

        #endregion

        #region DELETE /api/users/current/deleteCheckIn
        [HttpDelete]
        [SessionAuthorize]
        [CustomRoute("current/checkIn/{id:Guid}", 1, "mobile")]
        public async Task<IHttpActionResult> DeleteCheckIn(Guid id)
        {
            try
            {
                if (!this.ModelState.IsValid)
                {
                    return this.AsFailedResponse(Converters.ModelStateToApiErrorConverter(ModelState), HttpStatusCode.BadRequest);
                }

                var userId = this.User.Identity.GetUserId();

                var applicationUser = await this.ApplicationUserManager.FindByIdAsync(userId);

                if (applicationUser == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2003), HttpStatusCode.NotFound);
                }

                var checkIn =
                    await
                        this.DataContext.Set<CheckIn>()
                            .FirstOrDefaultAsync(c => c.Id == id);

                if (checkIn == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2400), HttpStatusCode.NotFound);
                }
                else
                {
                    checkIn.IsDelete = true;
                }

                this.DataContext.SaveChanges();

                return this.AsSuccessResponse("Deleted Check In");
            }
            catch (Exception ex)
            {

                return this.AsFailedResponse(ex);
            }

        }

        #endregion

        #region POST /api/users/unlockProfile
        [HttpPost]
        [SessionAuthorize]
        [CustomRoute("unlockProfile", 1, "mobile")]
        public async Task<IHttpActionResult> UnlockProfile()
        {
            try
            {

                var userId = this.User.Identity.GetUserId();

                var applicationUser = await this.ApplicationUserManager.FindByIdAsync(userId);

                if (applicationUser == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2003), HttpStatusCode.NotFound);
                }
                applicationUser.IsUnlocked = true;

                await this.ApplicationUserManager.UpdateAsync(applicationUser);

                return this.AsSuccessResponse("Unlocked");
            }
            catch (Exception ex)
            {

                return this.AsFailedResponse(ex);
            }

        }

        #endregion

        

        #region Functions
        private TokenReturnModel GenerateLocalAccessTokenResponse(ApplicationUser user)
        {
            var tokenExpiration = TimeSpan.FromDays(Startup.TokenExipration);
            var tokenSessionExpiration = TimeSpan.FromDays(Startup.TokenSessionExipration);

            ClaimsIdentity identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);
            identity.AddClaim(new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
            var roles = ApplicationUserManager.GetRoles(user.Id);

            foreach (var role in roles)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, role));
            }

            var utcNow = DateTime.UtcNow;

            var props = new AuthenticationProperties()
            {
                IssuedUtc = utcNow,
                ExpiresUtc = utcNow.Add(tokenExpiration),
            };

            var ticket = new AuthenticationTicket(identity, props);

            var accessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);
            //var refreshToken = Startup.OAuthOptions.RefreshTokenFormat.Protect(ticket);

            var tokenResponse = new TokenReturnModel()
            {
                Username = user.UserName,
                Access_token = accessToken,
                Token_type = "bearer",
                Expires_in = tokenSessionExpiration.TotalSeconds.ToString(),
                Issued = ticket.Properties.IssuedUtc.ToString(),
                Expires = utcNow.Add(tokenSessionExpiration).ToString()
            };

            return tokenResponse;
        }
        #endregion

    }
    
}