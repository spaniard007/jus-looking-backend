﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlTypes;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using avb.Core.Models;
using avb.Web.AttributesCustom;
using avb.Web.Constants;
using avb.Web.Infrastructures;
using avb.Web.Models.BindingModels;
using avb.Web.Models.QueryModels;
using avb.Web.Models.ReturnModels;
using Caf.Core.Data;
using Caf.Web.WebApi.AttributesCustom;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace avb.Web.Controllers
{
    [RoutePrefix("api/conversations")]
    public class ConversationController : ApplicationCrudControllerBase<Conversation>
    {
        #region Get /api/user/current/conversations
        [HttpGet]
        [CustomRoute("~/api/users/current/conversations", 1, "mobile")]
        [SessionAuthorize]
        public async Task<IHttpActionResult> GetMyConversations(int? count = null, int? sinceId = null, int? maxId = null, int? page = null, string fields = null)
        {
            try
            {
                var userId = User.Identity.GetUserId();


                var queryStringStore = String.Format(@"

SELECT Id, A.MesSecondaryId AS SecondaryId,FromUserId,ToUserId,LastMessage,LastMessageType,A.MesCreatedDate AS ModifiedDate,FirstName,LastName,Avatar,RelationshipStatus FROM
(
	SELECT Conversation.*,Message.SecondaryId AS MesSecondaryId,Message.CreatedDate AS MesCreatedDate FROM
	(
		SELECT Conversation.*,AppUser.FirstName,AppUser.LastName,AppUser.Avatar,AppUser.RelationshipStatus 
		FROM (SELECT * FROM Conversation WHERE FromUserId = '{0}') Conversation
		INNER JOIN (SELECT Id AS UserId,FirstName,LastName,Avatar,RelationshipStatus FROM ApplicationUser ) AppUser 
		ON Conversation.ToUserId = AppUser.UserId 
	) Conversation
	INNER JOIN (SELECT MAX(SecondaryId) AS SecondaryId,ConversationId,MAX(CreatedDate) AS CreatedDate FROM Message GROUP BY ConversationId) Message
	ON Conversation.Id = Message.ConversationId
	WHERE Conversation.DeletedDateTimeFromer < Message.CreatedDate 
) AS A

UNION

SELECT Id,B.MesSecondaryId AS SecondaryId,FromUserId,ToUserId,LastMessage,LastMessageType,B.MesCreatedDate AS ModifiedDate,FirstName,LastName,Avatar,RelationshipStatus FROM
(
	SELECT Conversation.*,Message.SecondaryId AS MesSecondaryId,Message.CreatedDate AS MesCreatedDate FROM
	(
		SELECT Conversation.*,AppUser.FirstName,AppUser.LastName,AppUser.Avatar,AppUser.RelationshipStatus  
		FROM (SELECT * FROM Conversation WHERE ToUserId = '{0}') Conversation
		INNER JOIN (SELECT Id AS UserId,FirstName,LastName,Avatar,RelationshipStatus FROM ApplicationUser) AppUser 
		ON Conversation.FromUserId = AppUser.UserId 
	) Conversation
	INNER JOIN (SELECT MAX(SecondaryId) AS SecondaryId,ConversationId,MAX(CreatedDate) AS CreatedDate FROM Message GROUP BY ConversationId) Message
	ON Conversation.Id = Message.ConversationId
	WHERE Conversation.DeletedDateTimeToer < Message.CreatedDate 
) AS B", userId); 


                QueryResult queryResult = null;
                //fields = "Id,SecondaryId,FromUserId,ToUserId,LastMessage,LastMessageType,ModifiedDate,FirstName,LastName";
                var queryContext = QueryContext.Build()
                    .WithSql(queryStringStore)
                    .WithFields(fields)
                    .WithPaging(count, sinceId, maxId, page)
                    .Sort("modifiedDate", false);

                queryResult = this.QueryStore.Use(this.DataContext).QueryAll<dynamic>(queryContext);

                return this.AsSuccessResponse(queryResult);
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }
        }

        #endregion

        #region DELETE /api/user/current/conversations/delete/{Guid:conversationId}
        [HttpDelete]
        [CustomRoute("~/api/users/current/conversations/{id:Guid}", 1, "mobile")]
        [SessionAuthorize]
        public async Task<IHttpActionResult> DeleteConversation(Guid id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return this.AsFailedResponse(Converters.ModelStateToApiErrorConverter(ModelState), HttpStatusCode.BadRequest);
                }

                var userId = User.Identity.GetUserId();

                var conversation = this.DataContext.Set<Conversation>().FirstOrDefault(c => c.Id == id);

                if (conversation == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2300), HttpStatusCode.BadRequest);
                }
                bool isFromer = conversation.FromUserId.Equals(userId);

                if (isFromer)
                {
                    conversation.DeletedDateTimeFromer = new SqlDateTime(DateTime.UtcNow).Value;
                }
                else
                {
                    conversation.DeletedDateTimeToer = new SqlDateTime(DateTime.UtcNow).Value;
                }

                this.DataContext.Set<Conversation>().AddOrUpdate(conversation);
                this.DataContext.SaveChanges();

                return this.AsSuccessResponse("Deleted");
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }
        }

        #endregion
    }

  
}