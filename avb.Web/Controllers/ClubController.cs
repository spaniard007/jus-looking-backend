﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using avb.Core.Models;
using avb.Core.Models.MapModels;
using avb.Web.AttributesCustom;
using avb.Web.Constants;
using avb.Web.Models.ReturnModels;
using avb.Web.Services.MapService;
using Caf.Core.Data;
using Caf.Core.Extensions;
using Caf.Web.WebApi.AttributesCustom;
using Dapper;
using Newtonsoft.Json.Linq;

namespace avb.Web.Controllers
{
    [RoutePrefix("api/clubs")]
    public class ClubController : ApplicationControllerBase
    {
        private IMapService mMapService;
        protected IMapService MapService
        {
            get
            {
                return mMapService ?? (mMapService = new GoogleMapService());
            }
        }

        #region GET /api/clubs
        [HttpGet]
        [SessionAuthorize]
        [CustomRoute("",1,"mobile")]
        public async Task<IHttpActionResult> GetClubs(double lat, double lng,double? radius = 30000,string keyword = "",string nextPageToken = "")
        {
            try
            {

                var result = await this.MapService.GetClubsNearBy(lat, lng, radius.Value, keyword, nextPageToken);
                if (result.Status.Equals("OK"))
                {
                    List<ClubReturnModel> clbs = new List<ClubReturnModel>();
                    foreach (var place in result.Places)
                    {
                        clbs.Add(GetClubReturnModel(place));
                    }

                    var ids = String.Join(",", clbs.Select(club =>
                    {
                        return String.Format("'{0}'", club.Id);
                    }));

                    var queryStringStore = String.Format(@"
                                    SELECT Club.*,ISNULL(TEMP.UndefinedCount,0)UndefinedCount,ISNULL(TEMP.SingleCount,0)SingleCount,ISNULL(TEMP.RelationshipCount,0)RelationshipCount FROM
                                    (
                                    	SELECT CheckIn.ClubId, 
                                    	SUM(CASE when ApplicationUser.RelationshipStatus = {0} then 1 else 0 end) as UndefinedCount,
                                    	SUM(CASE when ApplicationUser.RelationshipStatus = {1} then 1 else 0 end) as SingleCount,
                                    	SUM(CASE when ApplicationUser.RelationshipStatus = {2} then 1 else 0 end) as RelationshipCount
                                    	FROM (SELECT * FROM CheckIn WHERE ClubId IN ({3}) AND DATEDIFF(SECOND,ModifiedDate,GETUTCDATE()) < {4} AND IsDelete = 0) CheckIn
                                    	INNER JOIN ApplicationUser
                                    	ON CheckIn.UserCheckedInId = ApplicationUser.Id
                                    	GROUP BY CheckIn.ClubId
                                    ) AS TEMP
                                    RIGHT JOIN (SELECT * FROM CLub WHERE Id IN ({3}))Club
                                    ON TEMP.ClubId = Club.Id", (int)RelationshipStatus.Undefined, (int)RelationshipStatus.Single, (int)RelationshipStatus.Relationship, ids, Constants.Constants.CheckInExpiration);


                    var queryResult = (await this.DataContext.Database.Connection.QueryAsync<dynamic>(queryStringStore)).ToList();

                    foreach (var clb in clbs)
                    {
                        var tmp = queryResult.FirstOrDefault(c => c.Id == clb.Id);
                        if (tmp != null)
                        {
                            clb.UndefinedCount = tmp.UndefinedCount;
                            clb.SingleCount = tmp.SingleCount;
                            clb.RelationshipCount = tmp.RelationshipCount;
                        }

                    }
                    return this.AsSuccessResponse(new { NextPageToken = result.NextPageToken, data = clbs, Status = result.Status });
                }

                return this.AsSuccessResponse(new { NextPageToken = result.NextPageToken, data = Array.Empty<dynamic>(), Status = result.Status });
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }

        }
        #endregion

        #region GET /api/clubs/{clubId:Guid}
        [HttpGet]
        [SessionAuthorize]
        [CustomRoute("{clubId}", 1, "mobile")]
        public async Task<IHttpActionResult> GetClub(string clubId)
        {
            try
            {
                var result = await this.MapService.GetClubDetail(clubId);
                if (result.Place == null)
                {
                    return this.AsFailedResponse(ErrorsDefine.Find(2200));
                }
                var queryStringStore = String.Format(@"
SELECT Club.Id,Club.CreatedAt,ISNULL(TEMP.UndefinedCount,0)UndefinedCount,ISNULL(TEMP.SingleCount,0)SingleCount,ISNULL(TEMP.RelationshipCount,0)RelationshipCount FROM
(
	SELECT CheckIn.ClubId, 
	SUM(CASE when ApplicationUser.RelationshipStatus = {0} then 1 else 0 end) as UndefinedCount,
	SUM(CASE when ApplicationUser.RelationshipStatus = {1} then 1 else 0 end) as SingleCount,
	SUM(CASE when ApplicationUser.RelationshipStatus = {2} then 1 else 0 end) as RelationshipCount
	FROM (SELECT * FROM CheckIn WHERE DATEDIFF(SECOND,ModifiedDate,GETUTCDATE()) < {3} AND IsDelete = 0 AND CheckIn.ClubId = '{4}') CheckIn
	INNER JOIN ApplicationUser
	ON CheckIn.UserCheckedInId = ApplicationUser.Id
	GROUP BY CheckIn.ClubId
) AS TEMP
RIGHT JOIN (SELECT * FROM Club WHERE Id = '{4}') Club
ON TEMP.ClubId = Club.Id", (int)RelationshipStatus.Undefined, (int)RelationshipStatus.Single, (int)RelationshipStatus.Relationship, Constants.Constants.CheckInExpiration,clubId);

                var queryResult = await this.DataContext.Database.Connection.QueryFirstOrDefaultAsync<dynamic>(queryStringStore);

                var clb = GetClubReturnModel(result.Place);
                if (queryResult != null)
                {
                    clb.UndefinedCount = queryResult.UndefinedCount;
                    clb.SingleCount = queryResult.SingleCount;
                    clb.RelationshipCount = queryResult.RelationshipCount;
                }

                return this.AsSuccessResponse(clb);
            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }
        }
        #endregion

        #region GET /api/clubs/{clubId:Guid}/users
        [HttpGet]
        [SessionAuthorize]
        [CustomRoute("{clubId}/users", 1, "mobile")]
        public async Task<IHttpActionResult> GetUsersForClub(string clubId, int? count = null, int? sinceId = null, int? maxId = null, int? page = null)
        {
            try
            {

                var queryStringStore = String.Format(@"
SELECT ApplicationUser.*,CheckIn.ModifiedAt,CheckIn.SecondaryId FROM
(
	SELECT CheckIn.ModifiedAt,CheckIn.UserCheckedInId, ROW_NUMBER() OVER (Order by ModifiedAt) AS SecondaryId FROM CheckIn WHERE DATEDIFF(SECOND,ModifiedDate,GETUTCDATE()) < {0} AND IsDelete = 0 AND CheckIn.ClubId = '{1}'
) CheckIn
INNER JOIN (SELECT Id,Avatar,RelationshipStatus FROM ApplicationUser) ApplicationUser
ON CheckIn.UserCheckedInId = ApplicationUser.Id", Constants.Constants.CheckInExpiration, clubId);


                var queryContext = QueryContext.Build()
                  .WithSql(queryStringStore)
                  .WithPaging(count, sinceId, maxId, page)
                  .Sort("modifiedAt", false);

                var queryResult = this.QueryStore.Use(this.DataContext).QueryAll<dynamic>(queryContext);

                return this.AsSuccessResponse(queryResult);

            }
            catch (Exception ex)
            {
                return this.AsFailedResponse(ex);
            }
        }
        #endregion

        #region Methods

        public ClubReturnModel GetClubReturnModel(Place place)
        {
              var  club = new ClubReturnModel()
                {
                    Id = place.Id,
                    Name = place.Name,
                    Vicinity = place.Vicinity,
                    Icon = place.Icon,
                };
                if (place.Photos != null)
                {
                    foreach (var photo in place.Photos)
                    {
                        club.Photos.Add(photo);
                    }
                   
                }
                if (place.Geometry != null)
                {
                    if (place.Geometry.Location != null)
                    {
                        club.Lat = place.Geometry.Location.Lat;
                        club.Lng = place.Geometry.Location.Lng;
                    }
                }
            
            return club;
        }

        public Club GetClub(Place place)
        {
            string id = place.Id;
            var club = this.DataContext.Set<Club>().FirstOrDefault(c => c.Id == id);
            if (club == null)
            {
                club = new Club()
                {
                    Id = place.Id,
                };
                return club;
            }
            return null;
        }

        #endregion
    }
}