﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using avb.Web.Services.Logging;
using Microsoft.AspNet.Identity;
using SendGrid;

namespace avb.Web.Services.EmailService
{
    public class SendGridEmailService : EmailService
    {
        #region Fields
        protected string mDisplayName;
        protected string mAccount;
        #endregion

        public SendGridEmailService()
            : base()
        {
            this.mDisplayName = ConfigurationManager.AppSettings["emailSendGridService:DisplayName"];
            this.mAccount = ConfigurationManager.AppSettings["emailSendGridService:Account"];
        }

        LoggingService log = new LogentriesLoggingService();

        #region Send

        public override Task SendAsync(IdentityMessage message)
        {
            try
            {
                var myMessage = new SendGridMessage();

                myMessage.AddTo(message.Destination);
                myMessage.From = new System.Net.Mail.MailAddress(mAddress, mDisplayName);
                myMessage.Subject = message.Subject;
                myMessage.Text = message.Body;
                myMessage.Html = message.Body;

                var credentials = new NetworkCredential(mAccount, mPassword);

                // Create a Web transport for sending email.
                var transportWeb = new SendGrid.Web(credentials);

                return transportWeb.DeliverAsync(myMessage);
               
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return Task.FromResult(0);
            }
        }

        #endregion
    }
}