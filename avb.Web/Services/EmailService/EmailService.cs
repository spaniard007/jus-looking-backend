﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;

namespace avb.Web.Services.EmailService
{
    public abstract class EmailService : IIdentityMessageService
    {
        #region Fields

        protected string mAddress;
        protected string mPassword;
        #endregion

        #region Send
        public abstract Task SendAsync(IdentityMessage message);
        #endregion

        #region Constructors
        protected EmailService(string address, string password)
        {
            mAddress = address;
            mPassword = password;
        }
        #endregion

        #region Constructors
        protected EmailService()
        {
            this.mAddress = ConfigurationManager.AppSettings["emailSendGridService:Address"];
            this.mPassword = ConfigurationManager.AppSettings["emailSendGridService:Password"];
        }
        #endregion

       
    }
}