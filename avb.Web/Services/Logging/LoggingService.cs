﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avb.Web.Services.Logging
{
    public abstract class LoggingService : ILoggingService
    {
        protected LoggingData GetLoggingData(object data)
        {
            try
            {
                LoggingData loggingData = new LoggingData(HttpContext.Current, data);

                return loggingData;
            }
            catch (Exception)
            {
                LoggingData loggingData = new LoggingData(data);

                return loggingData;
            }
        }

        public abstract void Log(object data);

        public abstract void Error(object data);
    }

    public class LoggingData
    {
        public string Path { get; set; }
        public string HttpMethod { get; set; }
        public string AcceptHeader { get; set; }
        public Object Data { get; set; }

        public LoggingData(HttpContext httpContext, Object data)
        {
            this.Path = httpContext.Request.Path;
            this.HttpMethod = httpContext.Request.HttpMethod;
            this.Data = data;
            this.AcceptHeader = string.Format("Header Accept: {0}", this.GetHeader(httpContext.Request, "accept"));
        }

        public LoggingData(Object data)
        {
            this.Path = "Log Function";
            this.HttpMethod = string.Empty;
            this.Data = data;
            this.AcceptHeader = string.Empty;
        }

        private string GetHeader(HttpRequest request, string headerName)
        {
            IEnumerable<string> headerValues = request.Headers.GetValues(headerName);
            return headerValues.FirstOrDefault();
        }
    }
}