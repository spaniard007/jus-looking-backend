﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using Newtonsoft.Json;

namespace avb.Web.Services.Logging
{
    public class LogentriesLoggingService : LoggingService
    {
        public override void Log(object data)
        {
            LoggingData loggingData = GetLoggingData(data);

            var logger = LogManager.GetLogger(string.Format("{0} {1}", loggingData.HttpMethod, loggingData.Path));
            logger.Info(JsonConvert.SerializeObject(loggingData));
        }

        public override void Error(object data)
        {
            LoggingData loggingData = GetLoggingData(data);

            var logger = LogManager.GetLogger(string.Format("{0} {1}", loggingData.HttpMethod, loggingData.Path));
            logger.Error(JsonConvert.SerializeObject(loggingData));
        }
    }    
}