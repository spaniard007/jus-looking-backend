﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avb.Web.Services.Logging
{
    public interface ILoggingService
    {
        void Log(object data);
        void Error(object data);
    }
}