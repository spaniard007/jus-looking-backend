﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using avb.Core.Models.MapModels;
using Flurl;
using Flurl.Http;

namespace avb.Web.Services.MapService
{
    public class GoogleMapService : IMapService
    {
        private string mApiKey;

        private const string NearBySearch = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
        private const string TextSearch = "https://maps.googleapis.com/maps/api/place/textsearch/json?";
        private const string Detail = "https://maps.googleapis.com/maps/api/place/details/json?";

        public GoogleMapService()
        {
            mApiKey = ConfigurationManager.AppSettings["GoogleMap.ApiKey"]; ;
        }
        public async Task<Result> GetClubsNearBy(double lat, double lng, double radius, string searchKey = "", string pageToken = "")
        {
            var query = new
            {
                location = lat + "," + lng,
                radius = radius,
                type = "night_club",
                query = String.IsNullOrEmpty(searchKey) ? "" : searchKey,
                pagetoken = String.IsNullOrEmpty(pageToken) ? "" : pageToken,
                key = mApiKey
            };
            if (String.IsNullOrEmpty(searchKey))
                return await NearBySearch.SetQueryParams(query).GetJsonAsync<Result>();
            return await TextSearch.SetQueryParams(query).GetJsonAsync<Result>();
        }

        public async Task<ResultDetail> GetClubDetail(string placeId)
        {
            var query = new
            {
                placeid = placeId,
                key = mApiKey
            };
            return await Detail.SetQueryParams(query).GetJsonAsync<ResultDetail>();
        }
    }
}