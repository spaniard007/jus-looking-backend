﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using avb.Core.Models.MapModels;

namespace avb.Web.Services.MapService
{
    public interface IMapService
    {
        Task<Result> GetClubsNearBy(double lat, double lng, double radius, string searchKey = "", string pageToken = "");

        Task<ResultDetail> GetClubDetail(string placeId);
    }
}