﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using avb.Web.Services.Logging;
using Flurl.Http;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;

namespace avb.Web.Services.PushNotification
{
    public class OneSignalService
    {
        #region Fields

        private string mOneSignalUri = ConfigurationManager.AppSettings["OneSignal.Uri"];
        private string mOneSignalAppId;
        private string mOneSignalApiKey;
        private const string IOS_BADGE_TYPE = "Increase";
        private const string IOS_BADGE_COUNT = "1";

        #endregion
      

        private LoggingService log = new LogentriesLoggingService();

        IFlurlClient OneSignalRequest()
        {
            return mOneSignalUri.WithHeader("Authorization", "Basic " + mOneSignalApiKey);
        }

        #region Constructor

        public OneSignalService(string oneSignalAppId, string oneSignalApiKey)
        {
            this.mOneSignalApiKey = oneSignalApiKey;
            this.mOneSignalAppId = oneSignalAppId;
        }

        public OneSignalService()
        {
            this.mOneSignalApiKey = ConfigurationManager.AppSettings["OneSignal.ApiKey"];
            this.mOneSignalAppId = ConfigurationManager.AppSettings["OneSignal.AppId"];
        }
        
        #endregion


        #region Methods

        public async Task PushNotificationForUsers(string[] playerIds,string headding,string content, string collapseId,object data, string icon)
        {
              try
            {
                var obj = new
                {
                    app_id = this.mOneSignalAppId,
                    headings = new {en = headding},
                    contents = new {en = content},
                    data = data,
                    collapse_id = collapseId,
                    ios_badgeType = IOS_BADGE_TYPE,
                    ios_badgeCount = IOS_BADGE_COUNT,
                    small_icon  = icon,
                    include_player_ids = playerIds
                };
                //var json = JsonConvert.SerializeObject(obj);
                await OneSignalRequest().PostJsonAsync(obj);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        #endregion
       
    }
}