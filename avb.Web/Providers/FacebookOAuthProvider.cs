﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using avb.Core.Models;
using Flurl.Http;

namespace avb.Web.Providers
{
    public class FacebookOAuthProvider : ExternalOAuthProviderBase
    {
        private string mAppToken = string.Empty;

        public override string OnGetProviderName()
        {
            return "Facebook";
        }

        public override async Task<ExternalOAuthProviderInfo> VerifyExternalAccessToken(string accessToken)
        {
            string verifyTokenEndPoint = string.Format("https://graph.facebook.com/debug_token?input_token={0}&access_token={1}", accessToken, mAppToken);

            dynamic dto = await verifyTokenEndPoint.GetJsonAsync();
            dynamic data = dto.data;

            ExternalOAuthProviderInfo info = new ExternalOAuthProviderInfo();
            info.UserId = data.user_id;
            info.AppId = data.app_id;

            if (!string.Equals(mProviderId, info.AppId, StringComparison.OrdinalIgnoreCase))
            {
                return null;
            }

            var profileEndPoint = string.Format("https://graph.facebook.com/me?fields=email,first_name,last_name,picture,gender");
            dynamic profileInfo = await profileEndPoint.WithOAuthBearerToken(accessToken).GetJsonAsync();

            try
            {
                info.Email = profileInfo.email;
            }
            catch (Exception)
            {
                info.Email = null;
                // not have info to update profile
            }


            try
            {
                info.Avatar = profileInfo.picture.data.url;
            }
            catch (Exception)
            {
                info.Avatar = null;
                // not have info to update profile
            }

            try
            {
                info.FirstName = profileInfo.first_name;
            }
            catch (Exception)
            {
                info.FirstName = null;
            }

            try
            {
                info.LastName = profileInfo.last_name;
            }
            catch (Exception)
            {
                info.LastName = null;
            }

            try
            {
                string gender = profileInfo.gender;
                info.Gender = gender.Equals("male") ? Gender.Male : Gender.Female;
            }
            catch (Exception)
            {
            }

            return info;
        }

        public FacebookOAuthProvider(string providerId, string appToken)
            : base(providerId)
        {
            mAppToken = appToken;
        }
    }
}