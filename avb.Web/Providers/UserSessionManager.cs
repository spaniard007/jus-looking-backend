﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using avb.Core.Data;
using avb.Core.Models;
using avb.Web.Infrastructures;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace avb.Web.Providers
{
    public class UserSessionManager
    {
        #region Fields
        private DataContext dataContext = null;
        private ApplicationUserManager applicationUserManager = null;
        private HttpRequest currentRequest = null;
        private TimeSpan tokenSessionExpiration = TimeSpan.FromDays(Startup.TokenSessionExipration);

        protected DataContext DataContext
        {
            get { return this.dataContext ?? HttpContext.Current.GetOwinContext().Get<DataContext>(); }
        }

        protected ApplicationUserManager ApplicationUserManager
        {
            get { return this.applicationUserManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }

        private HttpRequest CurrentRequest
        {
            get
            {
                return this.currentRequest ?? HttpContext.Current.Request;
            }
        }
        #endregion

        private string GetCurrentBearerAuthrorizationToken()
        {
            string authToken = null;
            var authorization = this.CurrentRequest.Headers["Authorization"];

            if (authorization != null && (authorization.Contains("Bearer") || authorization.Contains("bearer")))
            {
                authToken = authorization.Replace("Bearer ", "").Replace("bearer ", "");
            }
            else
            {
                authToken = this.CurrentRequest.QueryString["apiKey"];
            }

            return authToken;
        }

        private string GetCurrentUserId()
        {
            if (HttpContext.Current.User == null)
            {
                return null;
            }
            string userId = HttpContext.Current.User.Identity.GetUserId();
            return userId;
        }

        public void CreateUserSession(string userId, string authToken)
        {
            // role: 1 time only 1 user is login

            // get userSession
            var userSessionFirstOrDefault = this.DataContext.Set<UserSession>().FirstOrDefault(session => session.OwnerUserId == userId);

            if (userSessionFirstOrDefault != null)
            {
                userSessionFirstOrDefault.AuthToken = authToken;
                userSessionFirstOrDefault.ExpirationDateTime = DateTime.UtcNow.Add(tokenSessionExpiration);
            }
            else
            {
                this.DataContext.Set<UserSession>().Add(new UserSession()
                {
                    OwnerUserId = userId,
                    AuthToken = authToken,
                    ExpirationDateTime = DateTime.UtcNow.Add(tokenSessionExpiration)
                });
            }

            this.DataContext.SaveChanges();
        }

        public void InvalidateUserSession()
        {
            string authToken = GetCurrentBearerAuthrorizationToken();
            var currentUserId = GetCurrentUserId();
            var userSession = this.DataContext.Set<UserSession>().FirstOrDefault(session =>
                session.AuthToken == authToken && session.OwnerUserId == currentUserId);
            if (userSession != null)
            {
                this.DataContext.Set<UserSession>().Remove(userSession);
                this.DataContext.SaveChanges();
            }
        }

        public bool ReValidateSession()
        {
            string authToken = this.GetCurrentBearerAuthrorizationToken();
            var currentUserId = this.GetCurrentUserId();
            var userSession = this.DataContext.Set<UserSession>().FirstOrDefault(session =>
                session.AuthToken == authToken && session.OwnerUserId == currentUserId);

            if (userSession == null)
            {
                // User does not have a session with this token --> invalid session
                return false;
            }

            if (userSession.ExpirationDateTime < DateTime.UtcNow)
            {
                // User's session is expired --> invalid session
                return false;
            }

            // Extend the lifetime of the current user's session: current moment + fixed timeout
            userSession.ExpirationDateTime = DateTime.UtcNow.Add(tokenSessionExpiration);
            this.DataContext.SaveChanges();

            return true;
        }

        public void DeleteExpiredSessions()
        {
            var userSessions = this.DataContext.Set<UserSession>().Where(
                session => session.ExpirationDateTime < DateTime.UtcNow);
            this.DataContext.Set<UserSession>().RemoveRange(userSessions);
            this.DataContext.SaveChanges();
        }
    }
}