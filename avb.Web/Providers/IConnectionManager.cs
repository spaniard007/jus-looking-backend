﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using Microsoft.AspNet.Identity;

namespace avb.Web.Providers
{
    public interface IConnectionManager
    {
        Connection GetConnection(IPrincipal user);
        void AddConnection(IPrincipal user, string connectionId);
        void DropConnection(IPrincipal user, string connectionId);
        IList<Connection> GetActiveConnections();
    }

    public class InMemoryConnectionManager : IConnectionManager
    {
        // Singleton instance
        private readonly static Lazy<IConnectionManager> mInstance = new Lazy<IConnectionManager>(() => new InMemoryConnectionManager());
        public static IConnectionManager Instance
        {
            get
            {
                return mInstance.Value;
            }
        }

        private readonly ConcurrentDictionary<string, Connection> mConnections = new ConcurrentDictionary<string, Connection>();

        public Connection GetConnection(IPrincipal user)
        {
            var userId = user.Identity.GetUserId();

            Connection connection;
            mConnections.TryGetValue(userId, out connection);

            return connection;
        }

        public void AddConnection(IPrincipal user, string connectionId)
        {
            var userId = user.Identity.GetUserId();
            var username = user.Identity.GetUserName();

            if (string.IsNullOrEmpty(userId)) return;

            var connection = new Connection()
            {
                ConnectionId = connectionId,
                UserId = userId,
                Username = username
            };

            mConnections.TryAdd(userId, connection);
        }

        public void DropConnection(IPrincipal user, string connectionId)
        {
            var userId = user.Identity.GetUserId();
            var username = user.Identity.GetUserName();

            if (string.IsNullOrEmpty(userId)) return;

            Connection connection;
            mConnections.TryRemove(userId, out connection);
        }

        public IList<Connection> GetActiveConnections()
        {
            return mConnections.Values.ToList();
        }
    }

    public class Connection
    {
        public string ConnectionId { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public bool IsConnected { get; set; }
    }
}