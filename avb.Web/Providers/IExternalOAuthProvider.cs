﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using avb.Core.Models;

namespace avb.Web.Providers
{
    public interface IExternalOAuthProvider
    {
        string ProviderName { get; }
        Task<ExternalOAuthProviderInfo> VerifyExternalAccessToken(string accessToken);
    }

    public abstract class ExternalOAuthProviderBase : IExternalOAuthProvider
    {
        private string mProviderName = string.Empty;
        public string ProviderName
        {
            get
            {
                return OnGetProviderName();
            }
        }
        public abstract string OnGetProviderName();


        public abstract Task<ExternalOAuthProviderInfo> VerifyExternalAccessToken(string accessToken);

        protected string mProviderId;
        protected ExternalOAuthProviderBase(string providerId)
        {
            mProviderId = providerId;
        }
    }

    public class ExternalOAuthProviderInfo
    {
        public string UserId { get; set; }
        public string AppId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Avatar { get; set; }
    }
}