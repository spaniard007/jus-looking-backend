﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace avb.Web.Providers
{
    public interface IExternalOAuthProviderManager
    {
        Task<ExternalOAuthProviderInfo> VerifyExternalAccessToken(string providerName, string accessToken);
    }

    public class ExternalOAuthProviderManager : IExternalOAuthProviderManager
    {
        private IExternalOAuthProvider[] mProviders;

        public Task<ExternalOAuthProviderInfo> VerifyExternalAccessToken(string providerName, string accessToken)
        {
            if (mProviders == null) throw new NullReferenceException("IExternalOAuthProvider arrays is empty or not initialized");

            var provider = mProviders.FirstOrDefault(pid => String.Equals(pid.ProviderName, providerName, StringComparison.CurrentCultureIgnoreCase));
            if (provider == null) return null;

            return provider.VerifyExternalAccessToken(accessToken);
        }

        public ExternalOAuthProviderManager(params IExternalOAuthProvider[] providers)
        {
            mProviders = providers;
        }
    }
}