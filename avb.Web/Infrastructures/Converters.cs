﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;
using avb.Web.Constants;
using Caf.Web.WebApi.Infrastructures;

namespace avb.Web.Infrastructures
{
    public class Converters
    {
        public static List<ApiErrorMessage> ModelStateToApiErrorConverter(ModelStateDictionary modelState)
        {
            var listApiErrorMessage = new List<ApiErrorMessage>();

            foreach (var modelStateValue in modelState.Values)
            {
                foreach (var error in modelStateValue.Errors)
                {
                    var apiErrorMessage = new ApiErrorMessage();

                    try
                    {
                        if (!string.IsNullOrEmpty(error.ErrorMessage))
                        {
                            var errorCode = int.Parse(error.ErrorMessage);
                            var errorDefine = ErrorsDefine.Instance[errorCode];
                            apiErrorMessage.ErrorCode = errorCode;
                            apiErrorMessage.ErrorMessage = errorDefine;
                        }
                        else
                        {
                            apiErrorMessage.ErrorCode = 9999;
                            apiErrorMessage.ErrorMessage = error.Exception.Message;
                        }

                    }
                    catch (Exception ex)
                    {
                        apiErrorMessage.ErrorCode = 9999;
                        apiErrorMessage.ErrorMessage = error.ErrorMessage + ": " + ex.Message;
                    }

                    listApiErrorMessage.Add(apiErrorMessage);
                }
            }

            return listApiErrorMessage;
        }
    }
}