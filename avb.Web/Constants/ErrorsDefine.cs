﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Caf.Web.WebApi.Infrastructures;

namespace avb.Web.Constants
{
    public class ErrorsDefine
    {
        private IDictionary<int, string> errorCodeTable = new Dictionary<int, string>()
        {
            //9999: Server can not handle this error
            //9998: Error Identity
            
            //20xx: User Error
            {2000, "Username not empty"}, 
            {2001, "Password not empty"},
            {2002, "Email Address in invalid format"},
            {2003, "Email or password is incorrect"},
            {2004, "Your user was deleted"},
            {2005, "User not found"},

            //21xx: OAuth Error
            {2100, "Provider not empty"},
            {2101, "ExternalAccessToken not empty"}, 
            {2102, "Username not empty"}, 
            {2103, "Password not empty"}, 

            //22xx: Club Error
            {2200,"Club not found"},

            //23xx: Conversation Error
            {2300,"Conversation not found"},

            //24xx: Conversation Error
            {2400,"Checkin not found"},
        };

        public string this[int errorCode]
        {
            get { return errorCodeTable[errorCode]; }
        }

        private static ErrorsDefine instance;
        public static ErrorsDefine Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ErrorsDefine();
                }
                return instance;
            }
        }

        public static ApiErrorMessage Find(int errorCode)
        {
            var apiErrorMessage = new ApiErrorMessage();

            try
            {
                var errorDefine = Instance[errorCode];
                apiErrorMessage.ErrorCode = errorCode;
                apiErrorMessage.ErrorMessage = errorDefine;
            }
            catch (Exception ex)
            {
                apiErrorMessage.ErrorCode = 9999;
                apiErrorMessage.ErrorMessage = ex.Message;
            }

            return apiErrorMessage;
        }
    }
}