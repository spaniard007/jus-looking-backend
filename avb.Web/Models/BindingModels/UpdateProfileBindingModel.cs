﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using avb.Core.Models;
using Caf.Core.Models;

namespace avb.Web.Models.BindingModels
{
    public class UpdateProfileBindingModel
    {
        #region First Name

        public string FirstName { get; set; }

        #endregion

        #region Last Name

        public string LastName { get; set; }

        #endregion

        #region Gender

        public Gender Gender { get; set; }

        #endregion

        #region Avatar

        public string Avatar { get; set; }

        #endregion

        #region DOB
        public DateTime? DOB { get; set; }
        #endregion

        #region Bio

        public string Bio { get; set; }

        #endregion

        #region Hashtags
        public StringCollection Hashtags { get; set; }
        #endregion

        #region Photos
        public StringCollection Photos { get; set; }

        #endregion

        #region Relationship Status

        public RelationshipStatus RelationshipStatus { get; set; }

        #endregion

        #region PlayerId
        public string PlayerId { get; set; }
        #endregion

    }
}