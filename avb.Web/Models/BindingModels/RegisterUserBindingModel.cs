﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace avb.Web.Models.BindingModels
{
    public class RegisterUserBindingModel
    {
        [EmailAddress(ErrorMessage = "2002")]
        public string Email { get; set; }

        public string Password { get; set; }
    }
}