﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace avb.Web.Models.BindingModels
{
    public class LoginBindingModel
    {
        [EmailAddress(ErrorMessage = "2002")]
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}