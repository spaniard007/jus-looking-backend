﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avb.Web.Models.BindingModels
{
    public class DeleteCheckInBindingModel
    {
        public Guid CheckInId { get; set; }
    }
}