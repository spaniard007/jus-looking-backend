﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avb.Web.Models.BindingModels
{
    public class DeleteConversationBindingModel
    {
        public Guid ConversationId { get; set; }
    }
}