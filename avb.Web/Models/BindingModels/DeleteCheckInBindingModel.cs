﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avb.Web.Models.BindingModels
{
    public class CheckInBindingModel
    {
        public string ClubId { get; set; }

        public string Name { get; set; }
    }
}