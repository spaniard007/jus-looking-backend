﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Atoms.Web.Models.BindingModels
{
    public class RegisterExternalBindingModel
    {
        [Required(ErrorMessage = "2100")]
        public string Provider { get; set; }

        [Required(ErrorMessage = "2101")]
        public string ExternalAccessToken { get; set; }
    }
}