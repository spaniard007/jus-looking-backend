﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avb.Web.Models.BindingModels
{
    public class ChangePasswordBindingModel
    {
        public string Token { get; set; }

        public string NewPassword { get; set; }
    }
}