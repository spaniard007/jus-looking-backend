﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using avb.Core.Models;
using Caf.Core.Data;

namespace avb.Web.Models.ReturnModels
{
    public class MessageReturnModel
    {
        public Guid Id { get; set; }

        public Guid ConversationId { get; set; }

        public MessageType MessageType { get; set; }

        public MessageStatus Status { get; set; }

        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public string OwnerId { get; set; }

        public static List<MessageReturnModel> Create(QueryResult queryResult)
        {
            List<MessageReturnModel> results = new List<MessageReturnModel>();
            foreach (var result in queryResult.MultipleResults)
            {
                results.Add(MessageReturnModel.Create(result));
            }

            return results;
        }

        public static MessageReturnModel Create(dynamic data)
        {
            MessageReturnModel messageReturnModel = new MessageReturnModel()
            {
                Id = data.Id,
                ConversationId = data.ConversationId,
                MessageType = (MessageType)data.MessageType,
                Status = (MessageStatus)data.Status,
                Content = data.Content,
                CreatedDate = data.CreatedDate,
                ModifiedDate = data.ModifiedDate,
                OwnerId = data.OwnerId

            };
            return messageReturnModel;
        }
    }
}