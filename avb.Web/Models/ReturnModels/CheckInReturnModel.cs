﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avb.Web.Models.ReturnModels
{
    public class CheckInReturnModel
    {
        public Guid Id { get; set; }

        public Guid ClubId { get; set; }

        public Guid UserId { get; set; }

    }
}