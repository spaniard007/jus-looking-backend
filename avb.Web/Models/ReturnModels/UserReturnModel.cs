﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using avb.Core.Extensions;
using avb.Core.Models;
using Caf.Core.Models;

namespace avb.Web.Models.ReturnModels
{
    public class UserReturnModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public string Avatar { get; set; }

        public DateTime? DOB { get; set; }

        public string Bio { get; set; }

        public StringCollection Hashtags { get; set; }

        public StringCollection Photos { get; set; }

        public RelationshipStatus RelationshipStatus { get; set; }

        public bool IsUnlocked { get; set; }

        public string PlayerId { get; set; }

        public Status Status { get; set; }

        public static UserReturnModel Create(ApplicationUser applicationUser)
        {
            var userReturnModel = new UserReturnModel()
            {
                Id = applicationUser.Id,
                FirstName = applicationUser.FirstName,
                LastName = applicationUser.LastName,
                Email = applicationUser.Email,
                Avatar = applicationUser.Avatar,
                Bio = applicationUser.Bio,
                DOB = applicationUser.DOB,
                Gender = applicationUser.Gender,
                Hashtags = applicationUser.Hashtags,
                Photos = applicationUser.Photos,
                RelationshipStatus = applicationUser.RelationshipStatus,
                IsUnlocked = applicationUser.IsUnlocked,
                PlayerId = applicationUser.PlayerId,
                Status = applicationUser.Status

            };
            return userReturnModel;
        }

        public static dynamic Create(ApplicationUser applicationUser,Guid? conversationId)
        {
            var result = new
            {
                Id = applicationUser.Id,
                FirstName = applicationUser.FirstName,
                LastName = applicationUser.LastName,
                Email = applicationUser.Email,
                Avatar = applicationUser.Avatar,
                Bio = applicationUser.Bio,
                DOB = applicationUser.DOB,
                Gender = applicationUser.Gender,
                Hashtags = applicationUser.Hashtags,
                Photos = applicationUser.Photos,
                RelationshipStatus = applicationUser.RelationshipStatus,
                IsUnlocked = applicationUser.IsUnlocked,
                ConversationId = conversationId,
                Status = applicationUser.Status
            };
            return result;
        }
    }

}