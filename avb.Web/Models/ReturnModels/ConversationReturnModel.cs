﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using avb.Core.Models;
using avb.Web.Controllers;
using avb.Web.Models.QueryModels;
using Caf.Core.Data;

namespace avb.Web.Models.ReturnModels
{
    public class ConversationReturnModel
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string LastMessage { get; set; }

        public MessageType LastMessageType { get; set; }

        public DateTime ModifiedDate { get; set; }

        public static IList<ConversationReturnModel> Create(QueryResult queryResult)
        {
            List<ConversationReturnModel> results = new List<ConversationReturnModel>();
            foreach (var result in queryResult.MultipleResults)
            {
                results.Add(ConversationReturnModel.Create(result));
            }

            return results;
        }

        public static ConversationReturnModel Create(dynamic data)
        {
            ConversationReturnModel conversationReturnModel = new ConversationReturnModel()
            {
                Id = data.Id,
                FirstName = data.FirstName,
                LastName = data.LastName,
                LastMessage = data.LastMessage,
                LastMessageType = (MessageType)data.LastMessageType,
                ModifiedDate = data.ModifiedDate
            };
            return conversationReturnModel;
        }
    }
}