﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace avb.Web.Models.ReturnModels
{
    public class TokenReturnModel
    {
        public string Username { get; set; }
        public string Access_token { get; set; }
        public string Token_type { get; set; }
        public string Expires_in { get; set; }
        public string Issued { get; set; }
        public string Expires { get; set; }
    }
}