﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using avb.Core.Extensions;
using avb.Core.Models;
using avb.Core.Models.MapModels;
using Caf.Core.Data;

namespace avb.Web.Models.ReturnModels
{
    public class ClubReturnModel
    {
        #region Id
        public string Id { get; set; }
        #endregion

        #region Name
        public string Name { get; set; }
        #endregion

        #region Vicinity
        public string Vicinity { get; set; }
        #endregion

        #region Icon
        public string Icon { get; set; }
        #endregion

        #region Lat
        public double? Lat { get; set; }

        #endregion

        #region Lng

        public double? Lng { get; set; }

        #endregion

        #region Photos
       

        public List<Photo> Photos { get; set; }

        #endregion

        #region UndefinedCount
        public int UndefinedCount { get; set; }
        #endregion

        #region SingleCount
        public int SingleCount { get; set; }
        #endregion

        #region RelationshipCount
        public int RelationshipCount { get; set; }
        #endregion

        #region Constructor

        public ClubReturnModel()
        {
            this.Photos = new List<Photo>();
        }

        #endregion
    }


    //    public static ClubReturnModel Create(Club club)
    //    {
    //        ClubReturnModel clubReturnModel = new ClubReturnModel();
    //        clubReturnModel.Id = club.Id;
    //        clubReturnModel.Address = club.Address;
    //        clubReturnModel.Name = club.Name;
    //        if (club.CheckIns != null)
    //        {
    //            clubReturnModel.CheckedIns = new List<UserCheckedInReturnModel>();
    //            var checkIns = club.CheckIns.Where(c => !c.IsDelete && DateTime.UtcNow.Subtract(c.ModifiedDate).TotalSeconds < 90 * 60).OrderByDescending(c => c.ModifiedDate);
    //            clubReturnModel.UndefinedCount =
    //                checkIns.Count(c => c.UserCheckedIn.RelationshipStatus == RelationshipStatus.Undefined);
    //            clubReturnModel.SingleCount =
    //                 checkIns.Count(c => c.UserCheckedIn.RelationshipStatus == RelationshipStatus.Single);

    //            clubReturnModel.RelationshipCount =
    //                 checkIns.Count(c => c.UserCheckedIn.RelationshipStatus == RelationshipStatus.Relationship);

    //            foreach (var checkin in checkIns)
    //            {
    //                clubReturnModel.CheckedIns.Add(UserCheckedInReturnModel.Create(checkin));
    //            }
    //        }

    //        return clubReturnModel;
    //    }

    //}

    //public class UserCheckedInReturnModel
    //{
    //    public Guid Id { get; set; }

    //    public string UserId { get; set; }

    //    public string Avatar { get; set; }

    //    public RelationshipStatus RelationshipStatus { get; set; }

    //    public static UserCheckedInReturnModel Create(CheckIn checkIn)
    //    {
    //        return new UserCheckedInReturnModel()
    //        {
    //            Id = checkIn.Id,
    //            UserId = checkIn.UserCheckedInId,
    //            Avatar = checkIn.UserCheckedIn.Avatar,
    //            RelationshipStatus = checkIn.UserCheckedIn.RelationshipStatus
                
    //        };
    //    }
    //}
}