﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using avb.Core.Models;

namespace avb.Web.Models.QueryModels
{
    public class ConversationQueryModel
    {
        public Guid Id { get; set; }

        public string FromUserId { get; set; }

        public string ToUserId { get; set; }

        public string LastMessage { get; set; }

        public MessageType LastMessageType { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime ModifiedDate { get; set; }
    }
}