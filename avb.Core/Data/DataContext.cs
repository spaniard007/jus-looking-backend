﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using avb.Core.Migrations;
using avb.Core.Models;
using avb.Core.Models.MapModels;
using Caf.Core.Extensions;
using Caf.Core.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace avb.Core.Data
{
    public class DataContext : IdentityDbContext<ApplicationUser>
    {
        public DataContext()
            : base(@"AvbDatabase",throwIfV1Schema : false)
        {
            //Database.SetInitializer<DataContext>(new DbInitializer());
            Database.SetInitializer<DataContext>(new MigrateDatabaseToLatestVersion<DataContext, Configuration>(@"AvbDatabase"));
        }

        #region On Model Creating
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<ApplicationUser>().ToTable("ApplicationUser");
            modelBuilder.Entity<IdentityRole>().ToTable("AspNetRole");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("AspNetUserLogin");
            modelBuilder.Entity<IdentityUserRole>().ToTable("AspNetUserRole");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("AspNetUserClaim");

            modelBuilder.ComplexType<StringCollection>();
            modelBuilder.ComplexType<PhotoCollection>();

            modelBuilder.Ignore<GuidEntityBase>();
            modelBuilder.Ignore<EntityBase<Guid, long>>();
            modelBuilder.Entity<GuidEntityBase>().HasKey(entity => entity.Id)
                .Property(ent => ent.SecondaryId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            #region ApplicationUser

            modelBuilder.Entity<ApplicationUser>().Property(entity => entity.Hashtags.Serialized).HasColumnName("Hashtags");
            modelBuilder.Entity<ApplicationUser>().Property(entity => entity.Photos.Serialized).HasColumnName("Photos");

            #endregion

            #region UserSession

            modelBuilder.Entity<UserSession>().HasKey(entity => entity.Id)
                .Property(ent => ent.SecondaryId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<UserSession>()
              .HasRequired(entity => entity.OwnerUser)
              .WithMany(entity => entity.UserSessions)
              .HasForeignKey(entity => entity.OwnerUserId)
              .WillCascadeOnDelete(false);

            #endregion

            #region Club

            modelBuilder.Entity<Club>().HasKey(entity => entity.Id)
                .Property(ent => ent.SecondaryId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            #endregion

            #region CheckIn

            modelBuilder.Entity<CheckIn>().HasKey(entity => entity.Id)
               .Property(ent => ent.SecondaryId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<CheckIn>()
               .HasRequired(entity => entity.UserCheckedIn)
               .WithMany(entity => entity.CheckIns)
               .HasForeignKey(entity => entity.UserCheckedInId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<CheckIn>()
                .HasRequired(entity => entity.Club)
                .WithMany(entity => entity.CheckIns)
                .HasForeignKey(entity => entity.ClubId)
                .WillCascadeOnDelete(false);


            #endregion

            #region Conversation

            modelBuilder.Entity<Conversation>().HasKey(entity => entity.Id)
           .Property(ent => ent.SecondaryId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


            modelBuilder.Entity<Conversation>().HasRequired(entity => entity.FromUser)
              .WithMany(entity => entity.ConversationsFromUser)
              .HasForeignKey(entity => entity.FromUserId)
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<Conversation>().HasRequired(entity => entity.ToUser)
                .WithMany(entity => entity.ConversationsToUser)
                .HasForeignKey(entity => entity.ToUserId)
                .WillCascadeOnDelete(false);


            #region Message

            modelBuilder.Entity<Message>().HasKey(entity => entity.Id)
              .Property(ent => ent.SecondaryId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Message>().HasRequired(entity => entity.Conversation)
              .WithMany(entity => entity.Messages)
              .HasForeignKey(entity => entity.ConversationId)
              .WillCascadeOnDelete(false);

            #endregion

          
            #endregion
        }
        #endregion

        #region Save Changes
        public override int SaveChanges()
        {
            var entries = this.ChangeTracker.Entries<IAudit>();
            foreach (var entry in entries)
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedDate = HiResDateTime.UtcNow;
                        entry.Entity.ModifiedDate = HiResDateTime.UtcNow;
                        entry.Entity.CreatedAt = (long)(entry.Entity.CreatedDate.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                        entry.Entity.ModifiedAt = (long)(entry.Entity.ModifiedDate.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                        break;
                    case EntityState.Modified:
                        entry.Property(x => x.CreatedDate).IsModified = false;
                        entry.Property(x => x.CreatedAt).IsModified = false;
                        entry.Entity.ModifiedDate = HiResDateTime.UtcNow;
                        entry.Entity.ModifiedAt = (long)(entry.Entity.ModifiedDate.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                        break;
                }
            }

            try
            {
                return base.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex.InnerException.InnerException ?? ex.InnerException;
            }
        }
        #endregion

        #region Create
        public static DataContext Create()
        {
            return new DataContext();
        }
        #endregion
    }
}
