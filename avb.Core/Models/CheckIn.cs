﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Models;
using Newtonsoft.Json;

namespace avb.Core.Models
{
    public class CheckIn : GuidEntityBase
    {
        #region UserCheckedIn

        public virtual ApplicationUser UserCheckedIn { get; set; }

        public string UserCheckedInId { get; set; }

        #endregion

        #region Club

        public virtual Club Club { get; set; }

        public string ClubId { get; set; }

        #endregion

        public bool IsDelete { get; set; }
    }
        
}
