﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Models;

namespace avb.Core.Models
{
    public class UserSession : GuidEntityBase
    {
        #region OwnerUser
        public string OwnerUserId { get; set; }
        public virtual ApplicationUser OwnerUser { get; set; }
        #endregion

        #region AuthToken
        public string AuthToken { get; set; }
        #endregion

        #region ExpirationDateTime
        public DateTime ExpirationDateTime { get; set; }
        #endregion
    }
}
