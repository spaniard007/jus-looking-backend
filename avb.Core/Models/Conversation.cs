﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Models;

namespace avb.Core.Models
{
    public class Conversation : GuidEntityBase
    {

        #region FromUser

        public virtual ApplicationUser FromUser { get; set; }

        public string FromUserId { get; set; }

        private DateTime mDeletedDateTimeFromer = new SqlDateTime(new DateTime(1970, 1, 1)).Value;

        public DateTime DeletedDateTimeFromer
        {
            get
            {
                return mDeletedDateTimeFromer;
            }
            set
            {
                mDeletedDateTimeFromer = value;
            }
        }

        #endregion

        #region ToUser

        public virtual ApplicationUser ToUser { get; set; }

        public string ToUserId { get; set; }

        private DateTime mDeletedDateTimeToer = new SqlDateTime(new DateTime(1970, 1, 1)).Value;

        public DateTime DeletedDateTimeToer
        {
            get
            {
                return mDeletedDateTimeToer;
            }
            set
            {
                mDeletedDateTimeToer = value;
            }
        }
        #endregion

        public string LastMessage { get; set; }

        public MessageType LastMessageType { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        public Conversation() : base()
        {
            DeletedDateTimeFromer = new SqlDateTime(new DateTime(1970, 1, 1)).Value;
            DeletedDateTimeToer = new SqlDateTime(new DateTime(1970, 1, 1)).Value;
        }

    }
}
