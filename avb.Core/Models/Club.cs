﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using avb.Core.Models.MapModels;
using Caf.Core.Models;

namespace avb.Core.Models
{
    public class Club : EntityBase<string, long>
    {
        #region Name
        public string Name { get; set; }
        #endregion

        #region CheckIns
        public virtual ICollection<CheckIn> CheckIns { get; set; }

        #endregion

        #region Constructor

        public Club()
        {
        }

        #endregion
    }

    
}
