﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace avb.Core.Models
{
    public class ApplicationUser : IdentityUser
    {
        #region CreatedAt
        public long CreatedAt { get; set; }
        #endregion

        #region ModifiedAt
        public long ModifiedAt { get; set; }
        #endregion

        #region UserSessions
        public virtual ICollection<UserSession> UserSessions { get; set; }
        #endregion

        #region First Name

        public string FirstName { get; set; }

        #endregion

        #region Last Name

        public string LastName { get; set; }

        #endregion

        #region Gender

        public Gender Gender { get; set; }

        #endregion

        #region Avatar

        public string Avatar { get; set; }

        #endregion

        #region DOB
        public DateTime? DOB { get; set; }
        #endregion

        #region Bio

        public string Bio { get; set; }

        #endregion

        #region Hashtags
        private StringCollection mHashtags = new StringCollection();

        public StringCollection Hashtags
        {
            get
            {
                return mHashtags;
            }
            set
            {
                mHashtags = value;
            }
        }

        #endregion

        #region Photos
        private StringCollection mPhotos = new StringCollection();

        public StringCollection Photos
        {
            get
            {
                return mPhotos;
            }
            set
            {
                mPhotos = value;
            }
        }

        #endregion

        #region Relationship Status

        public RelationshipStatus RelationshipStatus { get; set; }

        #endregion

        #region CheckIns
        public virtual ICollection<CheckIn> CheckIns { get; set; }

        #endregion

        #region ConversationsFromUser

        public virtual ICollection<Conversation> ConversationsFromUser { get; set; }

        #endregion

        #region ConversationsToUser

        public virtual ICollection<Conversation> ConversationsToUser { get; set; }

        #endregion

        #region Status
        public Status Status { get; set; }

        #endregion

        #region IsUnlocked
        public bool IsUnlocked { get; set; }

        #endregion

        #region PlayerId

        public string PlayerId { get; set; }

        #endregion

        #region Constructor

        public ApplicationUser()
        {
            var createdDate = new SqlDateTime(DateTime.UtcNow).Value;
            var modifiedDate = new SqlDateTime(DateTime.UtcNow).Value;
            this.CreatedAt = (long)(createdDate.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            this.ModifiedAt = (long)(modifiedDate.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        }

        #endregion
        
    }
    public enum Gender
    {
        Male = 0,
        Female = 1
    }

    public enum RelationshipStatus
    {
        Undefined = 1,
        Single = 2,
        Relationship = 4
    }
    public enum Status
    {
        Offline = 1,
        Online = 2
    }
}
