﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Models;

namespace avb.Core.Models
{
    public class Message : GuidEntityBase
    {

        #region Conversation

        public virtual Conversation Conversation { get; set; }

        public Guid ConversationId { get; set; }

        #endregion

        #region MessageType

        public MessageType MessageType { get; set; }

        #endregion

        #region MessageStatus

        public MessageStatus Status { get; set; }
        
        #endregion

        #region OwnerId

        public string OwnerId { get; set; }

        #endregion

        #region Content

        public string Content { get; set; }

        #endregion
    }
    public enum MessageType
    {
        Text = 1,
        Image = 2
    }

    public enum MessageStatus
    {
        Sent = 1,
        Seen = 2
    }
}
