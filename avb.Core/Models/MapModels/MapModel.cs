﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Models;
using Newtonsoft.Json;

namespace avb.Core.Models.MapModels
{
    public class Result
    {
        [JsonProperty("next_page_token")]
        public string NextPageToken { get; set; }
        [JsonProperty("results")]
        public List<Place> Places { get; set; }
        public string Status { get; set; }
    }

    public class ResultDetail
    {
        [JsonProperty("result")]
        public Place Place { get; set; }
        public string Status { get; set; }
    }
    public class Place
    {
        [JsonProperty("place_id")]
        public string Id { get; set; }

        public string Icon { get; set; }

        public Geometry Geometry { get; set; }

        public string Name { get; set; }

        public List<Photo> Photos { get; set; }

        public string Vicinity { get; set; }
    }

    public class Geometry
    {
        public Location Location { get; set; }
    }

    public class Location
    {
        public double Lat { get; set; }

        public double Lng { get; set; }
    }

    public class Photo
    {
        public int Height { get; set; }
        public int Width { get; set; }

        [JsonProperty("photo_reference")]
        public string PhotoReference { get; set; }
    }

    public class PhotoCollection : EntitySerializableCollection<Photo>
    {
        
    }
}
