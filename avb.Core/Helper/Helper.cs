﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Caf.Core.Extensions;
using Caf.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace avb.Core.Helper
{
    public class Helper
    {
        public static TDestination FromDynamic<TDestination>(TDestination entity, dynamic data)
        {
            JObject jObject = data as JObject;


            IList<string> keys = jObject.Properties().Select(p => p.Name).ToList();

            Type destinationType = typeof(TDestination);

            PropertyInfo[] properties = destinationType.GetProperties();

            foreach (var propertyInfo in properties)
            {
                var key = keys.FirstOrDefault(k => k.Equals(propertyInfo.Name, StringComparison.OrdinalIgnoreCase));
                if (key != null)
                {
                    var token = jObject[key];
                    if (typeof(StringCollection).IsAssignableFrom(propertyInfo.PropertyType))
                    {
                        propertyInfo.SetValue(entity, new StringCollection() { Serialized = token.ToString() });
                        continue;
                    }
                    if (typeof(DateTime?).IsAssignableFrom(propertyInfo.PropertyType))
                    {
                        DateTime? result = token.ToObject<DateTime?>();
                        propertyInfo.SetValue(entity, result == null ? (DateTime?)null : new SqlDateTime(result.Value.ToUniversalTime()).Value);
                        continue;
                    }

                    propertyInfo.SetValue(entity, token.GetValue());
                }
            }
            return entity;
        }
    }
}
