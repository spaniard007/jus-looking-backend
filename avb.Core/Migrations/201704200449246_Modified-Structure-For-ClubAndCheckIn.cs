namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedStructureForClubAndCheckIn : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CheckIn", "ClubId", "dbo.Club");
            DropIndex("dbo.CheckIn", new[] { "ClubId" });
            DropPrimaryKey("dbo.Club");
            AlterColumn("dbo.CheckIn", "ClubId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Club", "Id", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Club", "Id");
            CreateIndex("dbo.CheckIn", "ClubId");
            AddForeignKey("dbo.CheckIn", "ClubId", "dbo.Club", "Id");
            DropColumn("dbo.Club", "SearchString");
            DropColumn("dbo.Club", "Location");
            DropColumn("dbo.Club", "CoverPhoto");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Club", "CoverPhoto", c => c.String());
            AddColumn("dbo.Club", "Location", c => c.Geography());
            AddColumn("dbo.Club", "SearchString", c => c.String());
            DropForeignKey("dbo.CheckIn", "ClubId", "dbo.Club");
            DropIndex("dbo.CheckIn", new[] { "ClubId" });
            DropPrimaryKey("dbo.Club");
            AlterColumn("dbo.Club", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.CheckIn", "ClubId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Club", "Id");
            CreateIndex("dbo.CheckIn", "ClubId");
            AddForeignKey("dbo.CheckIn", "ClubId", "dbo.Club", "Id");
        }
    }
}
