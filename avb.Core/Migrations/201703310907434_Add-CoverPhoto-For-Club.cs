namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCoverPhotoForClub : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Club", "CoverPhoto", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Club", "CoverPhoto");
        }
    }
}
