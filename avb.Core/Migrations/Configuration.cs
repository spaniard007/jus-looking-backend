using avb.Core.Extensions;
using avb.Core.Models;

namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<avb.Core.Data.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(avb.Core.Data.DataContext context)
        {
            
        }
    }
}
