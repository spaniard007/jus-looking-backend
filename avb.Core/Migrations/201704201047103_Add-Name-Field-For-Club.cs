namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNameFieldForClub : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Club", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Club", "Name");
        }
    }
}
