// <auto-generated />
namespace avb.Core.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ModifiedClubTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ModifiedClubTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201704200739216_Modified-Club-Table"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
