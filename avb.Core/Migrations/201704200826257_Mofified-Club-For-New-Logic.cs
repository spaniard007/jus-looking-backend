namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MofifiedClubForNewLogic : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Club", "Name");
            DropColumn("dbo.Club", "Vicinity");
            DropColumn("dbo.Club", "Icon");
            DropColumn("dbo.Club", "Lat");
            DropColumn("dbo.Club", "Lng");
            DropColumn("dbo.Club", "Photos");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Club", "Photos", c => c.String());
            AddColumn("dbo.Club", "Lng", c => c.Double());
            AddColumn("dbo.Club", "Lat", c => c.Double());
            AddColumn("dbo.Club", "Icon", c => c.String());
            AddColumn("dbo.Club", "Vicinity", c => c.String());
            AddColumn("dbo.Club", "Name", c => c.String());
        }
    }
}
