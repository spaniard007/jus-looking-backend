namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsDeleteForCheckin : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CheckIn", "IsDelete", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CheckIn", "IsDelete");
        }
    }
}
