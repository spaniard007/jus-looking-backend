namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPlayerIdForUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ApplicationUser", "PlayerId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ApplicationUser", "PlayerId");
        }
    }
}
