namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedClubTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Club", "Icon", c => c.String());
            AddColumn("dbo.Club", "Lat", c => c.Double());
            AddColumn("dbo.Club", "Lng", c => c.Double());
            AddColumn("dbo.Club", "Photos", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Club", "Photos");
            DropColumn("dbo.Club", "Lng");
            DropColumn("dbo.Club", "Lat");
            DropColumn("dbo.Club", "Icon");
        }
    }
}
