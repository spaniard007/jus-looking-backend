namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AspNetRole",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRole",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRole", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.ApplicationUser",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        CreatedAt = c.Long(nullable: false),
                        ModifiedAt = c.Long(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Gender = c.Int(nullable: false),
                        Avatar = c.String(),
                        DOB = c.DateTime(),
                        Bio = c.String(),
                        Hashtags = c.String(),
                        Photos = c.String(),
                        RelationshipStatus = c.Int(nullable: false),
                        Location = c.Geography(),
                        Status = c.Int(nullable: false),
                        IsUnlocked = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.CheckIn",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserCheckedInId = c.String(nullable: false, maxLength: 128),
                        ClubId = c.Guid(nullable: false),
                        SecondaryId = c.Long(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        CreatedAt = c.Long(nullable: false),
                        ModifiedAt = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id, clustered: false)
                .ForeignKey("dbo.Club", t => t.ClubId)
                .ForeignKey("dbo.ApplicationUser", t => t.UserCheckedInId)
                .Index(t => t.UserCheckedInId)
                .Index(t => t.ClubId)
                .Index(t => t.SecondaryId, clustered: true, name: "CLUSTERED_INDEX_ON_SECONDARY_ID");
            
            CreateTable(
                "dbo.Club",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Address = c.String(),
                        SearchString = c.String(),
                        Location = c.Geography(),
                        SecondaryId = c.Long(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        CreatedAt = c.Long(nullable: false),
                        ModifiedAt = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id, clustered: false)
                .Index(t => t.SecondaryId, clustered: true, name: "CLUSTERED_INDEX_ON_SECONDARY_ID");
            
            CreateTable(
                "dbo.AspNetUserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Conversation",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FromUserId = c.String(nullable: false, maxLength: 128),
                        ToUserId = c.String(nullable: false, maxLength: 128),
                        LastMessage = c.String(),
                        LastMessageType = c.Int(nullable: false),
                        SecondaryId = c.Long(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        CreatedAt = c.Long(nullable: false),
                        ModifiedAt = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id, clustered: false)
                .ForeignKey("dbo.ApplicationUser", t => t.FromUserId)
                .ForeignKey("dbo.ApplicationUser", t => t.ToUserId)
                .Index(t => t.FromUserId)
                .Index(t => t.ToUserId)
                .Index(t => t.SecondaryId, clustered: true, name: "CLUSTERED_INDEX_ON_SECONDARY_ID");
            
            CreateTable(
                "dbo.Message",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ConversationId = c.Guid(nullable: false),
                        MessageType = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Content = c.String(),
                        SecondaryId = c.Long(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        CreatedAt = c.Long(nullable: false),
                        ModifiedAt = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id, clustered: false)
                .ForeignKey("dbo.Conversation", t => t.ConversationId)
                .Index(t => t.ConversationId)
                .Index(t => t.SecondaryId, clustered: true, name: "CLUSTERED_INDEX_ON_SECONDARY_ID");
            
            CreateTable(
                "dbo.AspNetUserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserSession",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OwnerUserId = c.String(nullable: false, maxLength: 128),
                        AuthToken = c.String(),
                        ExpirationDateTime = c.DateTime(nullable: false),
                        SecondaryId = c.Long(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        CreatedAt = c.Long(nullable: false),
                        ModifiedAt = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id, clustered: false)
                .ForeignKey("dbo.ApplicationUser", t => t.OwnerUserId)
                .Index(t => t.OwnerUserId)
                .Index(t => t.SecondaryId, clustered: true, name: "CLUSTERED_INDEX_ON_SECONDARY_ID");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserSession", "OwnerUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.AspNetUserRole", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.AspNetUserLogin", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.Conversation", "ToUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.Message", "ConversationId", "dbo.Conversation");
            DropForeignKey("dbo.Conversation", "FromUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.AspNetUserClaim", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.CheckIn", "UserCheckedInId", "dbo.ApplicationUser");
            DropForeignKey("dbo.CheckIn", "ClubId", "dbo.Club");
            DropForeignKey("dbo.AspNetUserRole", "RoleId", "dbo.AspNetRole");
            DropIndex("dbo.UserSession", "CLUSTERED_INDEX_ON_SECONDARY_ID");
            DropIndex("dbo.UserSession", new[] { "OwnerUserId" });
            DropIndex("dbo.AspNetUserLogin", new[] { "UserId" });
            DropIndex("dbo.Message", "CLUSTERED_INDEX_ON_SECONDARY_ID");
            DropIndex("dbo.Message", new[] { "ConversationId" });
            DropIndex("dbo.Conversation", "CLUSTERED_INDEX_ON_SECONDARY_ID");
            DropIndex("dbo.Conversation", new[] { "ToUserId" });
            DropIndex("dbo.Conversation", new[] { "FromUserId" });
            DropIndex("dbo.AspNetUserClaim", new[] { "UserId" });
            DropIndex("dbo.Club", "CLUSTERED_INDEX_ON_SECONDARY_ID");
            DropIndex("dbo.CheckIn", "CLUSTERED_INDEX_ON_SECONDARY_ID");
            DropIndex("dbo.CheckIn", new[] { "ClubId" });
            DropIndex("dbo.CheckIn", new[] { "UserCheckedInId" });
            DropIndex("dbo.ApplicationUser", "UserNameIndex");
            DropIndex("dbo.AspNetUserRole", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRole", new[] { "UserId" });
            DropIndex("dbo.AspNetRole", "RoleNameIndex");
            DropTable("dbo.UserSession");
            DropTable("dbo.AspNetUserLogin");
            DropTable("dbo.Message");
            DropTable("dbo.Conversation");
            DropTable("dbo.AspNetUserClaim");
            DropTable("dbo.Club");
            DropTable("dbo.CheckIn");
            DropTable("dbo.ApplicationUser");
            DropTable("dbo.AspNetUserRole");
            DropTable("dbo.AspNetRole");
        }
    }
}
