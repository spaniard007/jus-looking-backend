// <auto-generated />
namespace avb.Core.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddNameFieldForClub : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddNameFieldForClub));
        
        string IMigrationMetadata.Id
        {
            get { return "201704201047103_Add-Name-Field-For-Club"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
