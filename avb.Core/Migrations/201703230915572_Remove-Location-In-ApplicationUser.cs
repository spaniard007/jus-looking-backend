namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveLocationInApplicationUser : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ApplicationUser", "Location");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ApplicationUser", "Location", c => c.Geography());
        }
    }
}
