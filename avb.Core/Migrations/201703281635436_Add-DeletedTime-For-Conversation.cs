namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeletedTimeForConversation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Conversation", "DeletedDateTimeFromer", c => c.DateTime(nullable: false));
            AddColumn("dbo.Conversation", "DeletedDateTimeToer", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Conversation", "DeletedDateTimeToer");
            DropColumn("dbo.Conversation", "DeletedDateTimeFromer");
        }
    }
}
