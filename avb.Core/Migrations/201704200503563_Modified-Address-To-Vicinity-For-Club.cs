namespace avb.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedAddressToVicinityForClub : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Club", "Vicinity", c => c.String());
            DropColumn("dbo.Club", "Address");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Club", "Address", c => c.String());
            DropColumn("dbo.Club", "Vicinity");
        }
    }
}
